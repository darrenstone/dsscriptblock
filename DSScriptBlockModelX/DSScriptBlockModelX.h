//
//  DSScriptBlockModelX.h
//  DSScriptBlockModelX
//
//  Created by Darren Stone on 2018-06-20.
//  Copyright © 2018 Darren Stone. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for DSScriptBlockModelX.
FOUNDATION_EXPORT double DSScriptBlockModelXVersionNumber;

//! Project version string for DSScriptBlockModelX.
FOUNDATION_EXPORT const unsigned char DSScriptBlockModelXVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DSScriptBlockModelX/PublicHeader.h>


