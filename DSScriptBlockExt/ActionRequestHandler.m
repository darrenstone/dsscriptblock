//
//  ActionRequestHandler.m
//  DSScriptBlockExt
//
//  Created by Darren Stone on 2015-08-24.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

#import "ActionRequestHandler.h"

@import DSScriptBlockModel;
@import MobileCoreServices;

@interface ActionRequestHandler ()

@end

@implementation ActionRequestHandler

- (void)beginRequestWithExtensionContext:(NSExtensionContext *)context
{
    NSItemProvider* attachment = nil;

    BlockerModel* model = [BlockerModel newSharedModel];
    if (model.enabled)
    {
        NSData* jsonData = [model jsonDataAndReturnError:nil];

        if (jsonData)
        {
            attachment = [[NSItemProvider alloc] initWithItem:jsonData typeIdentifier:(id)kUTTypeJSON];
        }
        else
        {
            attachment = [[NSItemProvider alloc] initWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"blockerList" withExtension:@"json"]];

        }

        NSExtensionItem *item = [[NSExtensionItem alloc] init];
        item.attachments = @[attachment];
        
        [context completeRequestReturningItems:@[item] completionHandler:nil];
    }
    else
    {
        [context completeRequestReturningItems:nil completionHandler:nil];
    }
}

@end
