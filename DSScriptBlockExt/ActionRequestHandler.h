//
//  ActionRequestHandler.h
//  DSScriptBlockExt
//
//  Created by Darren Stone on 2015-08-24.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionRequestHandler : NSObject <NSExtensionRequestHandling>

@end
