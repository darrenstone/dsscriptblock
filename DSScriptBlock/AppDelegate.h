//
//  AppDelegate.h
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-08-24.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

#import <UIKit/UIKit.h>
@import DSScriptBlockModel;

@class CloudManager, UrlListController, Reachability;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    BlockerModel* _model;
    CloudManager* _cloudManager;
    UrlListController* _listController;
    Reachability* _reachability;
}

@property (strong, nonatomic) UIWindow *window;
- (BlockerModel*)model;
- (CloudManager*)cloudManager;

@property (nonatomic, readonly, weak) UIApplication* app; // Working around Xcode bugs

- (BOOL)canSendMailFromPresentingViewController:(UIViewController*)presentingController;

@property (nonatomic, readonly) BOOL isInternetAvailable;

@end

extern AppDelegate* g_appDelegate;

