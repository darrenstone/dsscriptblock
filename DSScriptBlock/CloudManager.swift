//
//  CloudManager.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2016-10-05.
//  Copyright © 2016 Darren Stone. All rights reserved.
//

import Foundation
import CloudKit
#if os(iOS)
import DSScriptBlockModel
#else
import DSScriptBlockModelX
#endif



//@objc protocol CloudManagerDelegate: NSObjectProtocol
//{
//    @objc(cloudManager:didLogMessage:)
//    func cloudManager(_ cloudManager: CloudManager, didLogMessage message: String)
//}

@objcMembers
class CloudManager : NSObject
{
    public enum SyncPolicy {
        case merge
        case replace
    }

    public enum Notifications
    {
        public static let syncStatusChanged = Notification.Name(rawValue: "CloudManagerSyncStatusChangedNotification")
    }

    enum Error: Int, Swift.Error {
        case iCloudNotAvailable = 1
        case iCloudDisabled = 2
        case unexpectedSubscriptionType = 3
        case internetNotAvailable = 4
        case syncInProgress = 5
        case userCancelled = 6
        case unexpected = 7

        var localizedDescription: String {
            return "This is the description"
        }
    }

    private var _serverChangeToken: CKServerChangeToken?
    private var _clientChangeToken = Int64(0)
    private var _blockerModel: BlockerModel
    private var _rootCollectionRecord: CKRecord?
    private var _subscription: CKRecordZoneSubscription?

    private var _container = CKContainer(identifier: "iCloud.ca.darrensoft.DSScriptBlock")
    private lazy var _config: CKOperation.Configuration = {
        let config = CKOperation.Configuration()
        config.container = _container
        return config
    }()

    private lazy var _db: CKDatabase = _container.privateCloudDatabase

    private var _zone: CKRecordZone?
    private var _rootModel: CKRecord?

    #if os(iOS)
    private let _reachability = Reachability.forInternetConnection()
    #endif
    public private(set) var syncInProgress = false
    private var _cancelled = false
    private var _currentOperation: CKOperation?
    private var _needsFetch = false

    private struct RecordType {
        static let rootModel = "RootModel"
        static let urlModel = "UrlModel"
        static let deletedModel = "DeletedModel"
    }

    var enabled: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "iCloudEnabled")
        }
    }

    private(set) var lastSyncSuccess = false
    private(set) var lastSyncError: Swift.Error?
    private(set) var lastSyncHadChanges = false

    private func resetLastSyncState() {
        lastSyncSuccess = false
        lastSyncError = nil
        lastSyncHadChanges = false
    }

    func setEnabled(enabled: Bool, completion:((Swift.Error?)->())?)
    {
        guard enabled != self.enabled else
        {
            completion?(nil)
            return
        }

        if !enabled
        {
            UserDefaults.standard.set(false, forKey: "iCloudEnabled")
            _serverChangeToken = nil
            _rootModel = nil
            _zone = nil
            _subscription = nil
            completion?(nil)
        }
        else
        {
            self.isCloudAvailable() { (available) in
                guard available else {
                    completion?(Error.iCloudNotAvailable)
                    return
                }

                UserDefaults.standard.set(true, forKey: "iCloudEnabled")
                completion?(nil)
            }
        }
    }

    // MARK: - Public

    @objc(initWithModel:)
    init(model: BlockerModel)
    {
        self._blockerModel = model

        if let prefs = UserDefaults.standard.dictionary(forKey: "CloudManager")
        {
            _serverChangeToken = DSUtil.deserializeObject(fromData: prefs["ServerToken"] as? Data)
            _clientChangeToken = prefs["ClientToken"] as? Int64 ?? 0
            _zone = DSUtil.deserializeObject(fromData: prefs["Zone"] as? Data)
            _rootCollectionRecord = DSUtil.deserializeObject(fromData:prefs["RootCollectionRecord"] as? Data)
            _subscription = DSUtil.deserializeObject(fromData: prefs["Subscription"] as? Data)
            _needsFetch = prefs["NeedsFetch"] as? Bool ?? false
        }
    }

    public func save()
    {
        var prefs = [String:Any]()

        if let serverToken = _serverChangeToken
        {
            prefs["ServerToken"] = DSUtil.serialize(object: serverToken)
        }
        if let zone = _zone
        {
            prefs["Zone"] = DSUtil.serialize(object: zone)
        }
        if let rootCollectionRecord = _rootCollectionRecord
        {
            prefs["RootCollectionRecord"] = DSUtil.serialize(object: rootCollectionRecord)
        }
        if let subscription = _subscription
        {
            prefs["Subscription"] = DSUtil.serialize(object: subscription)
        }
        prefs["ClientToken"] = NSNumber(value: _clientChangeToken)
        prefs["NeedsFetch"] = _needsFetch as NSNumber

        UserDefaults.standard.set(prefs, forKey: "CloudManager")
    }

    public var needsSync: Bool
    {
        return self.enabled && (self._blockerModel.needsCloudSync || self._needsFetch)
    }

    #if os(iOS)
    public func beginBackgroundSync(completion: (()->())?)
    {
        DSLog("MGR Starting Background sync")
        self.sync(syncPolicy: .merge) { (error) in
            DSLog("MGR Background Sync finished with error: \(String(describing: error))")
            completion?()
        }
    }
    #endif

    public func cancel()
    {
        DSLog("MGR Cancelling...")
        self._currentOperation?.cancel()
    }

    public func applicationShoudQuit() -> Bool {
        if !self.enabled {
            return true
        } else if self.syncInProgress {
            return false
        } else if _pendingSyncRequest {
            self.scheduleSync(timeInterval:0)
            return false
        } else {
            return true
        }
    }

    private var _pendingSyncRequest = false
    private var _pendingSyncTimer: Timer?
    public func scheduleSync(timeInterval: TimeInterval = 3)
    {
        DispatchQueue.main.async {
            self._pendingSyncTimer?.invalidate()
            self._pendingSyncTimer = nil;

            self._pendingSyncRequest = true

            let beginSync = {
                guard !self.syncInProgress else {
                    return
                }
                self.sync(syncPolicy: .merge, completion: nil)
            }

            if timeInterval > 0 {
                self._pendingSyncTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false, block: { (timer) in
                    self._pendingSyncTimer = nil
                    beginSync()
                })
            } else {
                beginSync()
            }
        }
    }

    public func sync(syncPolicy: SyncPolicy, completion: ((Swift.Error?)->())?)
    {
        self.resetLastSyncState()

        _pendingSyncRequest = false
        _pendingSyncTimer?.invalidate()
        _pendingSyncTimer = nil

        guard self.enabled else
        {
            completion?(Error.iCloudDisabled)
            self.lastSyncError = Error.iCloudDisabled
            NotificationCenter.default.post(name: Notifications.syncStatusChanged, object: self)
            return
        }

        #if os(iOS)
        guard _reachability?.currentReachabilityStatus() != NotReachable else
        {
            completion?(Error.internetNotAvailable)
            self.lastSyncError = Error.internetNotAvailable
            NotificationCenter.default.post(name: Notifications.syncStatusChanged, object: self)
            return
        }
        #endif

        guard !self.syncInProgress else
        {
            completion?(Error.syncInProgress)
            self.lastSyncError = Error.syncInProgress
            NotificationCenter.default.post(name: Notifications.syncStatusChanged, object: self)
            return
        }

        _cancelled = false

        if syncPolicy == .replace
        {
            _serverChangeToken = nil
            _blockerModel.purge()
        }

        #if os(iOS)
        g_appDelegate?.app?.isNetworkActivityIndicatorVisible = true
        #endif
        self.syncInProgress = true
        NotificationCenter.default.post(name: Notifications.syncStatusChanged, object: self)

        let finally = { (didChange: Bool, error: Swift.Error?) in
            DispatchQueue.main.async {
                self.lastSyncError = error
                self.lastSyncHadChanges = didChange
                self.save()
                completion?(error)
                #if os(iOS)
                g_appDelegate?.app?.isNetworkActivityIndicatorVisible = false
                #endif
                self.syncInProgress = false
                NotificationCenter.default.post(name: Notifications.syncStatusChanged, object: self)
                if self._pendingSyncRequest {
                    self.scheduleSync()
                }
            }
        }


        guard _serverChangeToken != nil else
        {
            DSLog("MGR No server token.  Performing initial sync")
            self.initialSync { (needsSync, error) in
                if let error = error
                {
                    DSLog("MGR Initial sync failed.  \(error)")
                    self._serverChangeToken = nil
                    finally(false, error)
                    return
                }

                assert(self._serverChangeToken != nil)
                if needsSync && self._serverChangeToken != nil
                {
                    DSLog("MGR Pending changes after initial sync.  Performing regular sync.")
                    self.syncInProgress = false
                    self.sync(syncPolicy: .merge, completion: completion)
                }
                else
                {
                    DSLog("MGR Initial sync complete.  No pending changes.")
                    finally(false, nil)
                }
            }
            return
        }


        let changelist: BlockerModel.Changelist = _blockerModel.changelist()

        DSLog("MGR Syncing modified records: \(changelist.modifiedRecords)")
        DSLog("MGR Syncing deleted records: \(changelist.deletedRecords)")

        let recordsToSave = changelist.modifiedRecords.map { return $0.ckRecord }
        let recordIdsToDelete = changelist.deletedRecords.map { return $0.ckRecordId }

        self.initializeDatabase { (error) in
            if let error = error
            {
                DSLog("MGR Couldn't get zone: \(error)")
                finally(false, error)
                return
            }

            self.send(deletions: recordIdsToDelete) { (error) in
                if let error = error
                {
                    DSLog("MGR Delete error: \(error)")
                    finally(false, error)
                    return;
                }

                self.send(updates: recordsToSave) { (error) in
                    if let error = error
                    {
                        DSLog("MGR Update error: \(error)")
                        finally(false, error)
                        return
                    }

                    self.fetch(
                        successCompletion: { (changedRecords, deletedRecordIds, clientToken) in

                            self.processChanges(changedRecords: changedRecords, deletedRecordIds: deletedRecordIds, clientToken: clientToken)
                            let hadChanges = changedRecords.count > 0 || deletedRecordIds.count > 0
                            finally(hadChanges, nil)

                        },
                        errorCompletion: { (error) in


                            let nsError = error as NSError
                            if nsError.domain == CKErrorDomain && nsError.code == CKError.changeTokenExpired.rawValue {
                                DSLog("Change Token Expired.")
                                self._serverChangeToken = nil
                                if syncPolicy == .merge {
                                    DSLog("Scheduling resync.")
                                    self.scheduleSync(timeInterval: 0)
                                }
                            }
                            else {
                                DSLog("MGR Fetch error: \(error)")
                            }
                            finally(false, error)

                    })
                }
            }
        }
    }

    public func resetCloud(completion: ((Swift.Error?)->())?)
    {
        guard self.enabled else
        {
            completion?(Error.iCloudDisabled)
            return
        }

        #if os(iOS)
        guard _reachability?.currentReachabilityStatus() != NotReachable else
        {
            completion?(Error.internetNotAvailable)
            return
        }
        #endif

        guard !self.syncInProgress else
        {
            completion?(Error.syncInProgress)
            return
        }

        DSLog("MGR Resetting cloud by deleting URL Zone")

        self.deleteZone() { (error) in
            guard error == nil else
            {
                completion?(error!)
                return
            }
            DSLog("MGR Reset complete.  Disabling iCloud")
            self.setEnabled(enabled: false, completion: { (_) in
                completion?(nil)
                return
            })
        }
    }

    private func deleteZone(completion: @escaping (Swift.Error?)->())
    {
        guard let zone = _zone else
        {
            completion(nil)
            return
        }

        DSLog("MGR Deleting zone \("zone.zoneID")")
        _db.delete(withRecordZoneID: zone.zoneID, completionHandler: { (zoneId, error) in
            if let zoneId = zoneId
            {
                DSLog("MGR Deleted zone \(zoneId)")
            }
            if let error = error
            {
                DSLog("MGR Delete zone failed: \(error)")
            }
            completion(error)
        })
    }

    public func setNeedsFetch()
    {
        self._needsFetch = true
    }

    // MARK: - Private

    private func isCloudAvailable(completion:@escaping (Bool)->())
    {
        _container.accountStatus { (status: CKAccountStatus, error: Swift.Error?) in
            if let error = error
            {
                DSLog("MGR Couldn't get account status: \(error)")
            }
            DispatchQueue.main.async() {
                completion(status == .available)
            }
        }
    }

    private func initializeSubscription(completion:@escaping (Swift.Error?)->())
    {
        guard _subscription == nil else
        {
            completion(nil)
            return
        }

        DSLog("MGR Fetching subscription")
        let op = CKFetchSubscriptionsOperation(subscriptionIDs: [CloudUtil.subscriptionId])
        op.configuration = _config
        op.database = _db
        op.fetchSubscriptionCompletionBlock = { (subscriptions, error) in
            self._currentOperation = nil

            guard error == nil else
            {
                let error = error!
                if error.isCancelledError()
                {
                    completion(Error.userCancelled)
                    return;
                }

                let ckError = error.normalizeCkErrorForRecordId(CloudUtil.subscriptionId)
                if ckError?.code == .unknownItem
                {
                    DSLog("MGR Subscription not found.  Creating.")
                    let subscription = CKRecordZoneSubscription(zoneID: CloudUtil.urlZoneId, subscriptionID: CloudUtil.subscriptionId)
                    subscription.recordType = CloudUtil.RecordType.urlModel
                    let note = CKSubscription.NotificationInfo()
                    note.shouldSendContentAvailable = true
                    subscription.notificationInfo = note

                    let op = CKModifySubscriptionsOperation(subscriptionsToSave: [subscription], subscriptionIDsToDelete: nil)
                    op.configuration = self._config
                    op.database = self._db
                    op.modifySubscriptionsCompletionBlock = { (subscriptions, _, error) in
                        self._currentOperation = nil

                        guard error == nil else
                        {
                            let error = error!
                            if error.isCancelledError()
                            {
                                completion(Error.userCancelled)
                                return
                            }

                            DSLog("MGR Couldn't create subscription: \(error)")
                            completion(error)
                            return
                        }

                        guard let subscription = subscriptions?.first else
                        {
                            DSLog("MGR Unexpected: Server did not return subscriptions")
                            completion(Error.unexpected)
                            return
                        }

                        guard let zoneSubscription = subscription as? CKRecordZoneSubscription else
                        {
                            DSLog("MGR Unexpected scription type: \(subscription)")
                            completion(Error.unexpected)
                            return
                        }
                        DSLog("MGR Created Subscription: \(zoneSubscription)")
                        self._subscription = zoneSubscription
                        completion(nil)
                        return
                    }

                    self._currentOperation = op
                    op.start()
                    return
                }

                DSLog("MGR Couldn't get subscription: \(error)")
                completion(error)
                return
            }

            guard let subscription = subscriptions?[CloudUtil.subscriptionId] else
            {
                DSLog("MGR Unexpected: Server did not return subscriptions")
                completion(Error.unexpected)
                return
            }

            guard let zoneSubscription = subscription as? CKRecordZoneSubscription else
            {
                DSLog("MGR Unexpected scription type: \(subscription)")
                completion(Error.unexpected)
                return
            }

            self._subscription = zoneSubscription
            completion(nil)
            return
        }

        self._currentOperation = op
        op.start()
    }

    private func initializeDatabase(completion: @escaping (Swift.Error?)->())
    {
        self.initializeZone { (error) in
            if let error = error
            {
                completion(error)
                return
            }

            self.initializeRootCollection(completion: { (error) in
                if let error = error
                {
                    completion(error)
                    return
                }

                self.initializeSubscription(completion: { (error) in
                    if let error = error
                    {
                        completion(error)
                        return
                    }
                    completion(nil)
                })
            })
        }
    }

    private func initializeRootCollection(completion:@escaping (Swift.Error?)->())
    {
        guard _rootCollectionRecord == nil else
        {
            completion(nil)
            return
        }

        DSLog("MGR Fetching Root Collection \(CloudUtil.defaultCollectionId.recordName)")
        let op = CKFetchRecordsOperation(recordIDs: [CloudUtil.defaultCollectionId])
        op.configuration = _config
        op.database = _db
        op.fetchRecordsCompletionBlock = { (records, error) in
            self._currentOperation = nil
            guard error == nil else
            {
                let error = error!
                if error.isCancelledError()
                {
                    completion(Error.userCancelled)
                    return;
                }

                let ckError = error.normalizeCkErrorForRecordId(CloudUtil.defaultCollectionId)
                if ckError?.code == .unknownItem
                {
                    DSLog("MGR Root Collection not found.  Creating.")
                    let rootCollectionRecord = CKRecord(recordType: CloudUtil.RecordType.defaultCollection, recordID: CloudUtil.defaultCollectionId)
                    rootCollectionRecord["name"] = "Root Collection"
                    let op = CKModifyRecordsOperation(recordsToSave: [rootCollectionRecord], recordIDsToDelete: nil)
                    op.configuration = self._config
                    op.database = self._db
                    op.modifyRecordsCompletionBlock = { (records, deletedIds, error) in
                        self._currentOperation = nil
                        guard error == nil else
                        {
                            let error = error!
                            if error.isCancelledError()
                            {
                                completion(Error.userCancelled)
                                return
                            }

                            DSLog("MGR Couldn't create root record: \(error)")
                            completion(error)
                            return
                        }

                        guard let record = records?.first else
                        {
                            DSLog("MGR Unexpected: Server did not return record")
                            completion(Error.unexpected)
                            return
                        }

                        self._rootCollectionRecord = record
                        completion(nil)
                    }
                    self._currentOperation = op
                    op.start()
                    return
                }
                else
                {
                    DSLog("MGR Couldn't fetch root collection partial error: \(String(describing: ckError))")
                }

                DSLog("MGR Couldn't fetch root collection: \(error)")
                completion(error)
                return
            }

            guard let records = records else
            {
                DSLog("MGR Unexpected: Server did not return records")
                completion(Error.unexpected)
                return
            }

            guard let record = records[CloudUtil.defaultCollectionId] else
            {
                DSLog("MGR Unexpected: Server did not return correct record: \(records)")
                completion(Error.unexpected)
                return
            }

            DSLog("MGR Fetched root collection: \(record)")
            self._rootCollectionRecord = record
            completion(nil)
            return
        }

        _currentOperation = op
        op.start()
    }

    private func initializeZone(completion:@escaping (Swift.Error?)->())
    {
        guard _zone == nil else
        {
            completion(nil)
            return;
        }

        DSLog("MGR Fetching Zone \(CloudUtil.urlZoneId.zoneName)")
        let op = CKFetchRecordZonesOperation(recordZoneIDs: [CloudUtil.urlZoneId])
        op.configuration = _config
        op.database = _db
        op.fetchRecordZonesCompletionBlock = { (zones, error) in
            self._currentOperation = nil

            guard error == nil else
            {
                let error = error!
                if error.isCancelledError()
                {
                    DSLog("MGR Fetch Zone User Cancelled")
                    completion(Error.userCancelled)
                    return
                }

                let ckError = error.normalizeCkErrorForRecordId(CloudUtil.urlZoneId)
                if ckError?.code == .zoneNotFound || ckError?.code == .userDeletedZone
                {
                    DSLog("MGR Creating zone \(CloudUtil.urlZoneId.zoneName)")
                    let zoneProto = CKRecordZone(zoneID: CloudUtil.urlZoneId)
                    let op = CKModifyRecordZonesOperation(recordZonesToSave: [zoneProto], recordZoneIDsToDelete: nil)
                    op.configuration = self._config
                    op.database = self._db
                    op.modifyRecordZonesCompletionBlock = { (zones, zoneIds, error) in
                        self._currentOperation = nil
                        guard error == nil else
                        {
                            let error = error!
                            if error.isCancelledError()
                            {
                                DSLog("MGR Fetch Zone User Cancelled")
                                completion(Error.userCancelled)
                                return
                            }
                            DSLog("MGR Couldn't create zone: \(error)")
                            completion(error)
                            return;
                        }

                        guard let zone = zones?.first else
                        {
                            DSLog("MGR Unexpected: Server did not return zones");
                            completion(Error.unexpected)
                            return;
                        }

                        DSLog("MGR Created zone \(zone)")
                        self._zone = zone
                        completion(nil)

                    }
                    self._currentOperation = op
                    op.start()
                    return
                }
                else
                {
                    DSLog("MGR Couldn't fetch zone: \(error) \(String(describing: ckError))")
                }

                completion(error)
                return
            }

            guard let zones = zones else
            {
                DSLog("MGR Unexpected: Server did not return zones");
                completion(Error.unexpected)
                return;
            }

            guard let zone = zones[CloudUtil.urlZoneId] else
            {
                DSLog("MGR Unexpected: Server returned unexpected zone: \(zones)");
                completion(Error.unexpected)
                return;
            }

            DSLog("MGR Got zone \(zone)")
            self._zone = zone
            completion(nil)
        }
        _currentOperation = op
        op.start()
    }

    private func initialSync(completion: ((Bool, Swift.Error?)->())?)
    {
        DSLog("MGR Performing Initial Sync")

        let finally = { (needsSync: Bool, error: Swift.Error?) in
            DispatchQueue.main.async {
                self.save()
                completion?(needsSync, error)
            }
        }

        self.initializeDatabase { (error) in
            if let error = error
            {
                DSLog("MGR Couldn't initialize database: \(error)")
                finally(false, error)
                return
            }

            self.fetch(
                successCompletion: { (changedRecords, deletedRecordIds, _) in

                    let needsSync = self._blockerModel.initialMerge(changedRecords: changedRecords, deletedRecordIds: deletedRecordIds)
                    finally(needsSync, nil)

            },
                errorCompletion: { (error) in

                    self._serverChangeToken = nil
                    DSLog("MGR Fetch error: \(error)")
                    finally(false, error)

            })
        }
    }

    private func send(deletions: [CKRecord.ID], completion: @escaping (Swift.Error?) -> ())
    {
        guard deletions.count > 0 else
        {
            DSLog("MGR No deletions to send.")
            completion(nil)
            return
        }

        DSLog("MGR Sending deletions: \(deletions.map({ $0.recordName }))")

        let op = CKModifyRecordsOperation(recordsToSave: nil, recordIDsToDelete: deletions)
        op.configuration = _config
        op.database = _db
        op.modifyRecordsCompletionBlock = { (savedRecords, deletedIds, error) in
            self._currentOperation = nil
            if let error = error
            {
                DSLog("MGR Delete error: \(error)")
                completion(error)
                return;
            }

            for deletedId in deletedIds!
            {
                DSLog("MGR Clearing sync flag for delected item \(deletedId.recordName)")
                self._blockerModel.deletedEntry(forId: deletedId)?.needsCloudSync = false
            }

            completion(nil)
        }
        self._currentOperation = op
        op.start()
    }

    private func nextClientChangeToken() -> Data
    {
        _clientChangeToken += 1
        return currentClientChangeToken()
    }

    private func currentClientChangeToken() -> Data
    {
        return Data(bytes:&_clientChangeToken, count:MemoryLayout.size(ofValue:_clientChangeToken))
    }

    private func send(updates: [CKRecord], completion: @escaping (Swift.Error?) -> ())
    {
        guard updates.count > 0 else
        {
            DSLog("MGR No updates to send.")
            completion(nil)
            return
        }

        DSLog("MGR Sending updates: \(updates.map({ $0.recordID.recordName }))")

        let op = CKModifyRecordsOperation(recordsToSave: updates, recordIDsToDelete: nil)
        op.configuration = _config
        op.database = _db
        op.clientChangeTokenData = self.nextClientChangeToken()
        DSLog("MGR New client change token: \(op.clientChangeTokenData?.int64Desc() ?? "NULL")")
        var retryRecords = [CKRecord]()

        op.perRecordCompletionBlock = { (record, error) in
            if let error = error
            {
                DSLog("MGR Record: \(record) Error: \(error)")
                if let ckError = error as? CKError
                {
                    if ckError.code == .serverRecordChanged
                    {
                        DSLog("MGR Conflict Detected.\n\n  Server: \(String(describing: ckError.serverRecord))\n\n  Client:\(String(describing: ckError.clientRecord))\n\n  Ancestor: \(String(describing: ckError.ancestorRecord))")
                        if let resolvedRecord = self._blockerModel.resolveConflict(ancestor: ckError.ancestorRecord, server: ckError.serverRecord, client: ckError.clientRecord)
                        {
                            DSLog("MGR Conflict Resolved.  Will retry \(resolvedRecord)")
                            retryRecords.append(resolvedRecord)
                        }
                    }
                }
            }
        }

        op.modifyRecordsCompletionBlock = { (savedRecords, deletedRecordsIds, error) in
            self._currentOperation = nil

            if error != nil && error!.isCancelledError()
            {
                completion(error)
                return
            }

            if retryRecords.count > 0
            {
                DSLog("MGR Retrying \(retryRecords)")
                self.send(updates: retryRecords, completion: completion)
            }
            else
            {
                var error : Swift.Error? = error
                DSLog("MGR Send complete.  Error: \(String(describing: error))")
                if let ckError = error as? CKError
                {
                    if ckError.code == .partialFailure
                    {
                        var skipAlert = true
                        ckError.partialErrorsByItemID?.values.forEach({ (itemError) in
                            let itemError = itemError as! CKError
                            if itemError.code != .serverRecordChanged
                            {
                                skipAlert = false
                            }
                        })
                        if skipAlert
                        {
                            error = nil
                        }
                    }
                }
                completion(error)
            }
        }
        self._currentOperation = op
        op.start()
    }

    private func fetch(successCompletion: @escaping ([CKRecord], [CKRecord.ID], Data?)->(), errorCompletion: @escaping (Swift.Error)->())
    {
        let zoneId = _zone!.zoneID
        let options = CKFetchRecordZoneChangesOperation.ZoneOptions()
        options.previousServerChangeToken = _serverChangeToken
        let op = CKFetchRecordZoneChangesOperation(recordZoneIDs: [zoneId], optionsByRecordZoneID: [zoneId: options])
        op.configuration = _config
        op.database = _db

        DSLog("MGR Fetching changes.\n\n  Last server token: \(String(describing: _serverChangeToken))\n\n  Last client token: \(self.currentClientChangeToken().int64Desc())")

        var changedRecords = [CKRecord]()
        var deletedRecordIds = [CKRecord.ID]()

        op.recordChangedBlock = { record in
            DSLog("MGR Record changed: \(record.recordType) \(record.recordID.recordName)")
            if record.recordType == CloudUtil.RecordType.urlModel
            {
                changedRecords.append(record)
            }
            else if record.recordType == CloudUtil.RecordType.defaultCollection
            {
                self._rootCollectionRecord = record
            }
        }

        op.recordWithIDWasDeletedBlock = { recordId, recordType in
            DSLog("MGR Record deleted: \(recordType) \(recordId.recordName)")
            if recordType == UrlModel.ckRecordType
            {
                deletedRecordIds.append(recordId)
            }
        }

        op.recordZoneChangeTokensUpdatedBlock = { zoneId, changeToken, clientToken in
            DSLog("MGR Server change token updated: \(String(describing: changeToken)) Client Token:\(String(describing: clientToken?.int64Desc()))")
            self._serverChangeToken = changeToken
        }

        op.recordZoneFetchCompletionBlock = { zoneId, serverToken, clientToken, moreComing, error in
            assert(!moreComing)

            self._currentOperation = nil

            DSLog("MGR Fetch completed.  Server Token: \(String(describing: serverToken)).  Client Token:\(String(describing: clientToken?.int64Desc()))")
            if let clientToken = clientToken
            {
                self.lastSyncSuccess = clientToken == self.currentClientChangeToken()
            }

            if let error = error
            {
                DSLog("MGR Fetch failed: \(error)")
                errorCompletion(error)
                return
            }

            self._serverChangeToken = serverToken
            self._needsFetch = false
            successCompletion(changedRecords, deletedRecordIds, clientToken)
        }
        self._currentOperation = op
        op.start()
    }

    private func processChanges(changedRecords: [CKRecord], deletedRecordIds: [CKRecord.ID], clientToken: Data?)
    {
        DSLog("MGR Processing changes")
        for record in changedRecords
        {
            DSLog("MGR Processing changed record: \(record.log())")
            self._blockerModel.process(changedCkRecord:record)
        }

        for id in deletedRecordIds
        {
            DSLog("MGR Processing deleted record: \(id.recordName)")
            self._blockerModel.process(deletedRecordId:id)
        }

        if self._blockerModel.isDirty
        {
            DSLog("MGR Saving local model")
            BlockerModel.saveSharedModel(self._blockerModel)
        }
    }
}

