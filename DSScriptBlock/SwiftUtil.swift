//
//  SwiftUtil.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2016-10-30.
//  Copyright © 2016 Darren Stone. All rights reserved.
//

import Foundation
import CloudKit

class DSUtil {
    class func deserializeObject<T: NSSecureCoding>(fromData data: Data?) -> T?
    {
        guard let data = data else
        {
            return nil
        }

        let coder = NSKeyedUnarchiver(forReadingWith: data)
        return T(coder: coder)
    }

    class func serialize(object: NSSecureCoding) -> NSData
    {
        let data = NSMutableData()
        let coder = NSKeyedArchiver(forWritingWith: data)
        object.encode(with: coder)
        coder.finishEncoding()
        return data as NSData
    }

    class func dispatchMainAfterDelay(_ delay: TimeInterval, handler: @escaping ()->()) {
        let deadlineTime = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(delay * 1000))
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: handler)
    }

    class func dispatchMain(handler: @escaping ()->()) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: handler)
    }
}

extension String {
    func hasCaseInsensitivePrefix(_ prefix: String) -> Bool {
        if let range = self.range(of: prefix, options: String.CompareOptions.caseInsensitive, range: nil, locale: nil) {
            return range.lowerBound == self.startIndex
        }
        return false
    }
}

extension Data
{
    func int64Desc() -> String
    {
        return (self as NSData).int64Desc()
    }
}

extension CKRecord
{
    func log() -> String
    {
        return "\(self.recordType): \(self.recordID.recordName)"
    }
}

extension Swift.Error
{
    func isCancelledError() -> Bool
    {
        if let ckError = self as? CKError
        {
            if ckError.code == .operationCancelled
            {
                return true
            }
        }
        if let cloudError = self as? CloudManager.Error
        {
            if cloudError == .userCancelled
            {
                return true
            }
        }
        return false
    }

    func partialErrorForRecordId(_ recordId: AnyHashable) -> CKError?
    {
        guard let ckError = self as? CKError else
        {
            return nil
        }

        guard  ckError.code == .partialFailure else
        {
            return nil
        }

        return ckError.partialErrorsByItemID?[recordId] as? CKError
    }

    func normalizeCkErrorForRecordId(_ recordId: AnyHashable) -> CKError?
    {
        guard let ckError = self as? CKError else
        {
            return nil
        }

        guard ckError.code == .partialFailure else
        {
            return ckError
        }

        guard let recordError: Error = ckError.partialErrorsByItemID?[recordId] else
        {
            return ckError
        }

        return (recordError as! CKError)
    }
}
