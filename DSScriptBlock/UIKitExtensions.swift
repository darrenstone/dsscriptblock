//
//  UIKitExtensions.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import UIKit

extension UIDevice
{
    class func isIPad() -> Bool
    {
        return UIDevice.current.userInterfaceIdiom == .pad
    }

    class func isIPhone() -> Bool
    {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
}
