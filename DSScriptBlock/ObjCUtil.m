//
//  ObjCUtil.m
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

#import "ObjCUtil.h"

@implementation NSObject(ObjCUtil)

- (void)ds_performSelector:(SEL)action withObject:(nullable id)obj
{
    [self performSelector:action withObject:obj];
}

@end


@implementation NSData(ObjCUtil)
- (NSString*)int64Desc
{
    if (self.length == sizeof(int64_t))
    {
        int64_t buffer = 0;
        [self getBytes:&buffer length:sizeof(buffer)];
        return [NSString stringWithFormat:@"%lld", buffer];
    }
    return @"Not an int.";
}
@end
