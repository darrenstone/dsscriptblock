//
//  ObjCUtil.h
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject(ObjCUtil)
- (void)ds_performSelector:(SEL)action withObject:(nullable id)obj;
@end

@interface NSData(ObjCUtil)
- (NSString*)int64Desc;
@end

NS_ASSUME_NONNULL_END
