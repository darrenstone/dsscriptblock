//
//  DSLog.m
//  DSScriptBlock
//
//  Created by Darren Stone on 2016-10-30.
//  Copyright © 2016 Darren Stone. All rights reserved.
//

#import "DSLog.h"
#import "CocoaLumberjack.h"
#import "DDLog+LOGV.h"

@interface DSLogManager : DDLogFileManagerDefault
@end

@implementation DSLogManager
- (id)init
{
#if defined(COCOA)
    return [super initWithLogsDirectory:nil];
#elif defined(TARGET_OS_IPHONE)
    return [super initWithLogsDirectory:nil defaultFileProtectionLevel:NSFileProtectionCompleteUntilFirstUserAuthentication];
#else
    return [super initWithLogsDirectory:nil];
#endif
}

- (NSString*)applicationName
{
    return @"DSSync";
}
@end

static DDFileLogger* gSyncFileLogger = nil;
DDLogLevel ddLogLevel = DDLogLevelInfo;

void DSLogInit()
{
    [DDLog removeAllLoggers];
    [DDLog addLogger:[DDASLLogger sharedInstance] withLevel:DDLogLevelInfo];
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:DDLogLevelInfo];
    NSCAssert(gSyncFileLogger == nil, @"");
    DSLogManager* syncLogManager = [DSLogManager new];
    gSyncFileLogger = [[DDFileLogger alloc] initWithLogFileManager:syncLogManager];
    gSyncFileLogger.rollingFrequency = 0.0;
    gSyncFileLogger.maximumFileSize = 1024 * 128; // 128 kB
    [DDLog addLogger:gSyncFileLogger withLevel:DDLogLevelInfo];

    NSLog(@"Logs Directory: %@", DSLogDirectory());
}

NSArray<NSString*>* DSLogPaths()
{
    return gSyncFileLogger.logFileManager.sortedLogFilePaths;
}

NSString* DSLogDirectory()
{
    return gSyncFileLogger.logFileManager.logsDirectory;
}

void DSLogV(NSString* format, ...)
{
    NSCAssert(gSyncFileLogger != nil, @"");

    va_list args;
    va_start(args, format);

    LOGV_MAYBE(NO, DDLogLevelInfo, DDLogFlagInfo, 0, nil, __PRETTY_FUNCTION__, format, args);
    va_end(args);
}

void DSLog(NSString* text)
{
    NSCAssert(gSyncFileLogger != nil, @"");
    DDLogInfo(@"%@", text);
}


@implementation DSLogWatcher

- (void)logMessage:(DDLogMessage *)logMessage
{
    [_delegate logMessage:logMessage->_message];
}

@synthesize enabled = _enabled;
- (void)setEnabled:(BOOL)enabled
{
    if (enabled != _enabled)
    {
        _enabled = enabled;
        if (!_enabled)
        {
            [DDLog removeLogger:self];
        }
        else
        {
            [DDLog addLogger:self withLevel:DDLogLevelInfo];
        }
    }
}

@end
