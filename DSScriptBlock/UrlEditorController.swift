//
//  UrlEditorController.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import UIKit
import DSScriptBlockModel

class UrlEditorController : UIViewController
{
    @IBOutlet private weak var _segmentControl: UISegmentedControl!
    @IBOutlet private weak var _textField: UITextField!
    @IBOutlet private weak var _notesView: UITextView!
    @IBOutlet weak var _pasteButton: UIButton!
    @IBOutlet weak var _domainTextField: UITextField!
    @IBOutlet weak var _warningLabel: UILabel!
    private weak var _returnTarget: AnyObject?
    private var _returnHandler: ((Any) -> ())?
    private var _originalUrl: UrlModel?
    private var _url : UrlModel?
    private(set) var canSave = false {
        didSet {
            _saveButton?.isEnabled = canSave
        }
    }

    private var blockerModel: BlockerModel { return g_appDelegate.model()! }

    var url: UrlModel? { return _url }
    var isDirty : Bool {
        guard let url = _url, let originalUrl = _originalUrl else {
            return false
        }
        return !url.matches(model: originalUrl)
    }
    private var _saveButton: UIBarButtonItem?

    private var _saveHandler: ((UrlModel)->())?

    func prepareUrl(_ url: UrlModel, saveHandler: @escaping (UrlModel)->())  {

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(didTapCancel(_:)))
        _saveButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(didTapDone(_:)))
        self.navigationItem.rightBarButtonItem = _saveButton
        _saveHandler = saveHandler
        self.setUrl(url)

    }

    @objc private func didTapCancel(_ sender: Any) {
        _saveHandler = nil
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    @objc private func didTapDone(_ sender: Any) {
        guard self.canSave else {
            return
        }
        if (self.isDirty) {
            _saveHandler?(self.url!)
        }
        _saveHandler = nil
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    func applyChanges() {
        guard let url = _url else {
            return
        }

        validateUrlString(_textField.text!)
        let text = _domainTextField.text! //_textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let notes = _notesView.text.trimmingCharacters(in: .whitespacesAndNewlines)

        url.notes = notes
        url.urlString = text
        url.urlType = self.urlType
    }

    var urlType = UrlModel.Kind.domainWhitelist {
        didSet {
            if !_isSegmentChanging {
                _segmentControl?.selectedSegmentIndex = urlType == .domainWhitelist ? 0 : 1
            }
        }
    }

    private var _isSegmentChanging = false
    @IBAction func segmentChanged(_ sender: Any) {
        _isSegmentChanging = true
        self.urlType = _segmentControl?.selectedSegmentIndex == 1 ? .domainBlacklist : .domainWhitelist
        _isSegmentChanging = false
        self.applyChanges()
    }

    func setReturnTarget<T: AnyObject>(_ target: T?, handler:((T)->())?)
    {
        guard let returnTarget = target, let returnHandler = handler else
        {
            _returnTarget = nil
            _returnHandler = nil
            return
        }

        _returnTarget = returnTarget
        _returnHandler = { (targ) in
            returnHandler(targ as! T)
        }
    }

    private func fireReturnTarget()
    {
        if let returnTarget = _returnTarget, let returnHandler = _returnHandler
        {
            returnHandler(returnTarget)
        }
    }

    private func validateUrlString(_ urlString: String) {
        guard let url = _url else {
            return
        }

        url.urlString = urlString
        url.urlType = self.urlType
        let result = self.blockerModel.validateUrl(model: url)
        _domainTextField?.text = result.0
        switch result.1 {
        case .ok :
            canSave = true
            _warningLabel?.text = ""
            break

        case .duplicateDomain :
            canSave = false
            _warningLabel?.text = "Domain already exists."
            break;

        case .invalidCharacters :
            canSave = false
            _warningLabel?.text = "Domain is invalid."
            break

        case .empty :
            canSave = false
            _warningLabel?.text = "Please enter a domain name or URL."
            break
        }
    }

    @IBAction func didTapPasteButton(_ sender: Any) {
        if let text = UIPasteboard.general.string {
            _textField.text = text
            validateUrlString(text)
        }
    }
}



/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - View Controller
//

extension UrlEditorController
{
    override func viewWillAppear(_ animated: Bool)
    {
        self.urlType = _url?.urlType ?? .domainWhitelist
        _textField.text = _url?.urlString ?? ""
        _notesView.text = _url?.notes ?? ""
        _textField.becomeFirstResponder()
        super.viewWillAppear(animated)
    }

}

/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - Text Field
//
extension UrlEditorController : UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.applyChanges()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        self.fireReturnTarget()

        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text: NSString = textField.text as NSString? else {
            return true
        }
        let newText = text.replacingCharacters(in: range, with: string)
        validateUrlString(newText as String)
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        validateUrlString("")
        return true
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - Private
//

extension UrlEditorController
{
    private func setUrl(_ newUrl: UrlModel?)
    {
        guard let url = newUrl else
        {
            self.title = ""
            _originalUrl = nil
            _url = nil;
            _textField?.text = ""
            _textField?.isEnabled = false
            _domainTextField?.text = ""
            _notesView?.text = ""
            _notesView?.isEditable = false
            _segmentControl?.isEnabled = false
            self.canSave = false
            self.isEditing = false
            return
        }

        _originalUrl = UrlModel(copy: url)
        _url = UrlModel(copy:url)
        switch (url.urlType)
        {
        case .domainBlacklist :
            self.title = "Domain Blacklist"
            break

        case .domainWhitelist :
            self.title = "Domain Whitelist"
            break

        case .urlBlacklist :
            self.title = "URL Blacklist"
            break
        }

        self.urlType = url.urlType

        let validation = self.blockerModel.validateUrl(model: url)
        self.canSave = validation.1 == .ok
        _textField?.text = url.urlString
        _textField?.isEnabled = true
        _domainTextField?.text = validation.0
        _notesView?.text = url.notes
        _notesView?.isEditable = true
        _segmentControl?.isEnabled = true
    }
}
