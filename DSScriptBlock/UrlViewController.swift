//
//  UrlViewController.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2020-11-20.
//  Copyright © 2020 Darren Stone. All rights reserved.
//

import UIKit

class UrlViewController: UIViewController {

    private enum SegueId : String
    {
        case editUrl = "EditUrlSegue"
    }


    @IBOutlet weak var _urlView: UITextField!
    @IBOutlet weak var _typeView: UITextField!
    @IBOutlet weak var _notesView: UITextView!
    @IBOutlet weak var _editButton: UIBarButtonItem!

    var saveHandler: ((UrlModel) -> ())?

    var url: UrlModel? {
        didSet {
            self.updateView()
        }
    }

    override func viewDidLoad() {
        self.updateView()
    }

    private func updateView() {
        guard self.isViewLoaded else {
            return
        }

        if let url = self.url {
            _urlView.text = url.urlString
            _typeView.text = url.urlType == UrlModel.Kind.domainWhitelist ? "Whitelist" : "Blacklist"
            _notesView.text = url.notes
            _editButton.isEnabled = true
        } else {
            _urlView.text = ""
            _typeView.text = ""
            _notesView.text = ""
            _editButton.isEnabled = false
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = SegueId(rawValue: segue.identifier ?? "") else { return }

        switch (identifier)
        {
            case .editUrl:
                guard let nav = segue.destination as? UINavigationController else { DSLog("EditUrlSegue expects navigation controller"); abort(); }
                guard let editor = nav.viewControllers[0] as? UrlEditorController else { DSLog("EditUrlSegue expects UrlEditorController"); abort(); }

                editor.prepareUrl(self.url!) { (newUrl) in
                    self.url = newUrl
                    self.saveHandler?(newUrl)
                }
        }
    }

}
