//
//  UrlListController.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import UIKit
import DSScriptBlockModel
import CloudKit

private func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - 1
//
class UrlListController : UITableViewController
{
    private enum SegueId : String
    {
        case viewUrl = "ViewUrlSegue"
        case newUrl = "NewUrlSegue"
        case about = "AboutSegue"
    }

    private var _loaded = false
    private var _whitelist = [UrlModel]()
    private var _blacklist = [UrlModel]()
    private var _filteredWhitelist = [UrlModel]()
    private var _filteredBlacklist = [UrlModel]()
    private var _model: BlockerModel!

    private enum Section: Int {
        case main = 0, blacklist, whitelist
    }

    private enum MainRow: Int {
        case about = 0, enabled
    }

    private let _sections : [Section] = [ .main, .blacklist, .whitelist ]
    private let _mainRows : [MainRow] = [ .about, .enabled ]

    private var _zoneId: CKRecordZone.ID?

    private var _cloudManager: CloudManager!
    private var _hasSyncNotification = false

    private var _searchController = UISearchController(searchResultsController: nil)
    private var _searchFilter = "" {
        didSet {
            updateFilteredLists()
            self.tableView.reloadData()
        }
    }

    @objc(processRemoteNotification:completion:)
    public func process(remoteNotification: [String:NSObject], completion: @escaping (UIBackgroundFetchResult)->())
    {
        let note = CKNotification(fromRemoteNotificationDictionary: remoteNotification)!
        if note.subscriptionID == CloudUtil.subscriptionId
        {
            _cloudManager.setNeedsFetch()
            DSLog("LST Background Sync beginning")
            _cloudManager.sync(syncPolicy: .merge, completion: { (error) in
                if let error = error
                {
                    DSLog("LST Background sync error: \(error)")
                    completion(UIBackgroundFetchResult.failed)
                }
                else
                {
                    DSLog("LST Background sync complete")
                    self.reloadData()
                    BlockerModel.saveSharedModel(self._model)
                    completion(UIBackgroundFetchResult.newData)
                }
            })
        }
    }
}

// ///////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - Search
//
extension UrlListController: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    private func setupSearch() {
        _searchController.delegate = self
        _searchController.searchResultsUpdater = self
        _searchController.dimsBackgroundDuringPresentation = false
        self.definesPresentationContext = true
        _searchController.searchBar.delegate = self
        self.navigationItem.searchController = _searchController
    }

    public func updateSearchResults(for searchController: UISearchController) {
        _searchFilter = searchController.searchBar.text ?? ""
    }

    private func updateFilteredLists() {
        guard !_searchFilter.isEmpty else {
            _filteredBlacklist = []
            _filteredWhitelist = []
            return
        }

        let query = _searchFilter.lowercased()
        _filteredWhitelist = _whitelist.filter { $0.urlString.hasCaseInsensitivePrefix(query) }
        _filteredBlacklist = _blacklist.filter { $0.urlString.hasCaseInsensitivePrefix(query) }
    }

    private var _currentBlacklist: [UrlModel] { return _searchFilter.isEmpty ? _blacklist : _filteredBlacklist }
    private var _currentWhitelist: [UrlModel] { return _searchFilter.isEmpty ? _whitelist : _filteredWhitelist }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - View Controller
//
extension UrlListController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()

        setupSearch()
        _model = g_appDelegate.model()
        _cloudManager = g_appDelegate.cloudManager()

        if !_hasSyncNotification
        {
            _hasSyncNotification = true
            var notificationToken: NSObjectProtocol?
            notificationToken = NotificationCenter.default.addObserver(forName: CloudManager.Notifications.syncStatusChanged, object: _cloudManager,queue: OperationQueue.main, using: { [weak self] (note) in
                if let strongSelf = self
                {
                    if !strongSelf._cloudManager.syncInProgress
                    {
                        DSLog("LST Sync Complete")
                        strongSelf.reloadData()
                    }
                }
                else
                {
                    NotificationCenter.default.removeObserver(notificationToken!)
                }
            })
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if !_loaded
        {
            self.reloadData()
            if !self.splitViewController!.isCollapsed {
                self.performSegue(withIdentifier: SegueId.about.rawValue, sender: self)
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        deselectTableView()
    }

    func deselectTableView() {
        guard self.splitViewController?.isCollapsed == true else {
            return
        }

        if let indexPath = self.tableView.indexPathForSelectedRow {
            DSUtil.dispatchMainAfterDelay(1) {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }

}

/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - Table View
//
extension UrlListController
{
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return _sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection sectionVal: Int) -> Int
    {
        let section = _sections[sectionVal]

        switch (section)
        {
        case .main :
            return _mainRows.count

        case .blacklist :
            return _currentBlacklist.count + 1

        case .whitelist :
            return _currentWhitelist.count + 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        struct CellId
        {
            static let plainCell = "PlainCell"
            static let switchCell = "SwitchCell"
            static let urlCell = "UrlCell"
            static let newItemCell = "NewItemCell"
            static let debugCell = "DebugCell"
        }

        let section = _sections[indexPath.section]
        var cell: UITableViewCell!

        switch (section)
        {
        case .main :
            let row = _mainRows[indexPath.row]
            switch (row)
            {
            case .about :
                cell = tableView.dequeueReusableCell(withIdentifier: CellId.plainCell, for: indexPath)
                cell.textLabel?.text = "About"

            case .enabled :
                cell = tableView.dequeueReusableCell(withIdentifier: CellId.switchCell, for: indexPath)
                cell.textLabel?.text = "Enabled"
                (cell.accessoryView as! UISwitch).isOn = _model.enabled
                cell.accessoryView!.tag = row.rawValue

            }
            break;

        case .blacklist :
            fallthrough
        case .whitelist :
            let list = section == .whitelist ? _currentWhitelist : _currentBlacklist
            if indexPath.row < list.count
            {
                cell = tableView.dequeueReusableCell(withIdentifier: CellId.urlCell, for: indexPath)
                let url = list[indexPath.row]
                cell.textLabel?.text = url.urlString
                cell.detailTextLabel?.text = url.needsCloudSync ? "Sync" : ""
            }
            else
            {
                cell = tableView.dequeueReusableCell(withIdentifier: CellId.newItemCell, for: indexPath)
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = self.view.tintColor!
                cell.textLabel?.text = "New Item"
                cell.detailTextLabel?.text = ""
            }
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        let section = _sections[indexPath.section]

        switch (section)
        {
        case .main :
            return false

        case .blacklist :
            return indexPath.row < _currentBlacklist.count

        case .whitelist :
            return indexPath.row < _currentWhitelist.count
        }
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        guard editingStyle == .delete else { return }

        let section = _sections[indexPath.section]
        switch (section)
        {
        case .whitelist :
            let url = _currentWhitelist[indexPath.row]
            self._model.removeUrl(url)
            if let index = _whitelist.firstIndex(of: url) {
                _whitelist.remove(at: index)
            }
            if let index = _filteredWhitelist.firstIndex(of: url) {
                _filteredWhitelist.remove(at: index)
            }
            break

        case .blacklist :
            let url = _currentBlacklist[indexPath.row]
            self._model.removeUrl(url)
            if let index = _blacklist.firstIndex(of: url) {
                _blacklist.remove(at: index)
            }
            if let index = _filteredBlacklist.firstIndex(of: url) {
                _filteredBlacklist.remove(at: index)
            }
            break

        case .main :
            break
        }
        tableView.deleteRows(at: [indexPath], with: .fade)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection sectionVal:Int) -> String
    {
        let section = _sections[sectionVal]
        switch (section)
        {
        case .whitelist :
            return "Domain Whitelist"

        case .blacklist :
            return "Domain Blacklist"

        default :
            return ""
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - Navigation
//
extension UrlListController
{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let saveHandler: (UrlModel) -> () = { (newUrl) in
            self._model.addUrl(newUrl)
            self.reloadData()
            self.selectUrl(newUrl)
        }

        guard let identifier = SegueId(rawValue: segue.identifier ?? "") else { return }

        switch (identifier)
        {
            case .viewUrl:
                guard let nav = segue.destination as? UINavigationController else { DSLog("ViewUrlSegue expects navigation controller"); abort(); }
                guard let viewer = nav.viewControllers[0] as? UrlViewController else { DSLog("ViewUrlSegue expects UrlViewController"); abort(); }
                viewer.url = self.selectedUrl()
                viewer.saveHandler = saveHandler

            case .newUrl :
                guard let nav = segue.destination as? UINavigationController else { DSLog("NewUrlSegue expects navigation controller"); abort(); }
                guard let editor = nav.viewControllers[0] as? UrlEditorController else { DSLog("NewUrlSegue expects UrlEditorController"); abort(); }

                let url = UrlModel()

                if sender is UITableViewCell {
                    let section = _sections[self.tableView.indexPathForSelectedRow!.section]
                    url.urlType = (section == .whitelist) ? .domainWhitelist : .domainBlacklist
                }
                else {
                    url.urlType = .domainWhitelist
                }
                editor.prepareUrl(url, saveHandler:saveHandler)

            case .about :
                guard let nav = segue.destination as? UINavigationController else { DSLog("AboutSegue expects navigation controller"); abort(); }
                guard let about = nav.viewControllers[0] as? AboutController else { DSLog("AboutSegue expect AboutController"); abort(); }
                about.cloudManager = self._cloudManager

        }
    }

    @objc private func popToUrlList(_ sender: Any?)
    {
        _ = self.navigationController?.popToViewController(self, animated: true)
    }

}

/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - Private
//
extension UrlListController
{
    private func reloadData()
    {
        _whitelist = _model.whitelistDomains
        _blacklist = _model.blacklistDomains
        _loaded = true
        updateFilteredLists()
        self.tableView?.reloadData()
    }

    private func selectUrl(_ url: UrlModel) {
        var indexPath: IndexPath?

        switch url.urlType {
        case .domainWhitelist :
            if let index = _currentWhitelist.firstIndex(of: url) {
                indexPath = IndexPath(row: index, section: Section.whitelist.rawValue)
            }
            break

        case .domainBlacklist :
            if let index = _currentBlacklist.firstIndex(of: url) {
                indexPath = IndexPath(row: index, section: Section.blacklist.rawValue)
            }
            break

        default :
            break
        }

        if let indexPath = indexPath {
            if self.tableView.indexPathsForVisibleRows?.contains(indexPath) == false {
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)

            if self.splitViewController?.isCollapsed == false {
                self.performSegue(withIdentifier: SegueId.viewUrl.rawValue, sender: self)
            }
            else {
                DSUtil.dispatchMainAfterDelay(1) {
                    self.deselectTableView()
                }
            }
        }


    }

    private func selectedUrl() -> UrlModel?
    {
        guard let selectedRow = self.tableView.indexPathForSelectedRow else { return nil }
        return self.urlForIndexPath(selectedRow)
    }

    private func urlForIndexPath(_ indexPath: IndexPath) -> UrlModel?
    {
        let section = _sections[indexPath.section]
        var list : [UrlModel]?

        switch (section)
        {
        case .whitelist :
            list = _currentWhitelist;
            break;

        case .blacklist :
            list = _currentBlacklist
            break

        default :
            break
        }

        if indexPath.row < list?.count
        {
            return list?[indexPath.row]
        }

        return nil
    }

    @IBAction private func didChangeEnabledSwitch(_ sender: UISwitch)
    {
        if let row = MainRow(rawValue: sender.tag)
        {
            switch row
            {
            case .enabled :
                _model.enabled = sender.isOn
                print("Enabled: \(_model.enabled)")
                break

            default :
                break
            }
        }
    }
}







