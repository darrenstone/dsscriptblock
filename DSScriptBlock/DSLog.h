//
//  DSLog.h
//  DSScriptBlock
//
//  Created by Darren Stone on 2016-10-30.
//  Copyright © 2016 Darren Stone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CocoaLumberjack.h"

NS_ASSUME_NONNULL_BEGIN

void DSLogInit();
NSArray<NSString*>* DSLogPaths();
NSString* DSLogDirectory();
void DSLogV(NSString* format, ...);
void DSLog(NSString* text);

extern DDLogLevel ddLogLevel;

@protocol DSLogWatcherDelegate<NSObject>
- (void)logMessage:(NSString*)text;
@end

@interface DSLogWatcher : DDAbstractLogger
@property (nonatomic, readwrite) BOOL enabled;
@property (nonatomic, weak) id<DSLogWatcherDelegate> delegate;
@end


NS_ASSUME_NONNULL_END
