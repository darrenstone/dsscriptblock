//
//  AboutController.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-07.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import UIKit
import CloudKit

class AboutController: UIViewController {

    @IBOutlet private weak var _versionLabel: UILabel!

    override func viewDidLoad() {
        guard let plist = Bundle.main.infoDictionary else {
            _versionLabel.text = "No plist"
            return
        }
        
        let shortVersion = plist["CFBundleShortVersionString"] as? String ?? "Unknown"
        let version = plist["CFBundleVersion"] as? String ?? "Unknown"

        #if DEBUG
        _versionLabel.text = "Version: \(shortVersion) (\(version)) DEBUG"
        #else
        _versionLabel.text = "Version: \(shortVersion) (\(version))"
        #endif
    }


    public var cloudManager: CloudManager!

    @IBOutlet private weak var _iCloudSwitch: UISwitch!

    override func viewWillAppear(_ animated: Bool)
    {
        assert(self.cloudManager != nil, "Caller must set cloud manager.")

        _iCloudSwitch.isOn = self.cloudManager.enabled
    }

    @IBAction private func didChangeSwitch(_ sender: UISwitch!)
    {
        self.cloudManager.setEnabled(enabled: sender.isOn) { (error) in
            if let error = error
            {
                var message: String?
                var title: String?
                if error as? CloudManager.Error == CloudManager.Error.iCloudNotAvailable
                {
                    title = "Sign in to iCloud"
                    message = "Sign in to your iCloud account to write records. On the Home screen, launch Settings, tap iCloud, and enter your Apple ID. Turn iCloud Drive on. If you don't have an iCloud account, tap Create a new Apple ID."
                }
                else
                {
                    title = "Couldn't enable iCloud"
                    message = "Error: \(error)"
                }
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel) { (action) in
                    sender.isOn = false
                })
                self.present(alert, animated: true, completion: nil)
                return
            }

            if self.cloudManager.enabled
            {
                let alert = UIAlertController(title:
                    nil, message: nil, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Merge", style: .default) { (action) in
                    self.beginSync(syncPolicy:.merge)
                })
                alert.addAction(UIAlertAction(title: "Replace", style: .destructive) { (action) in
                    self.beginSync(syncPolicy:.replace)
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                })
                alert.popoverPresentationController?.sourceView = sender
                alert.popoverPresentationController?.sourceRect = sender.bounds
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @IBAction private func didTapSyncNow(_ sender: Any?)
    {
        self.beginSync(syncPolicy: .merge)
    }


    @IBAction private func didTapLogFiles(_ sender: Any?)
    {
        let controller = RTDiagnosticsViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func didTapResetCloud(_ sender: Any?)
    {
        let alert = UIAlertController(title: "Reset iCloud", message: "iCloud database content will be deleted and iCloud syncing will be turned off.  Content on device will remain.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Reset", style: UIAlertAction.Style.destructive, handler: { (action) in
            self.cloudManager.resetCloud() { (error: Error?) in
                guard error == nil else
                {
                    self.presentError(error!)
                    return
                }
                self._iCloudSwitch.isOn = self.cloudManager.enabled
            }
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Sync

    private var _logViewer: LogViewController?

    private func beginSync(syncPolicy: CloudManager.SyncPolicy)
    {
        let logViewer = LogViewController()
        _logViewer = logViewer
        logViewer.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissLogViewer(sender:)))
        logViewer.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelSync(sender:)))
        let nav = UINavigationController(rootViewController: logViewer)
        self.present(nav, animated: true) {
            self.cloudManager.sync(syncPolicy: syncPolicy) { (error) in
                if let error = error
                {
                    self.presentError(error)
                }
            }
        }
    }

    private func presentError(_ error: Swift.Error)
    {
        if let ckError = error as? CKError
        {
            if ckError.code == .partialFailure
            {
                var skipAlert = true
                ckError.partialErrorsByItemID?.values.forEach({ (itemError) in
                    let itemError = itemError as! CKError
                    if itemError.code != .serverRecordChanged
                    {
                        skipAlert = false
                    }
                })
                if skipAlert
                {
                    return
                }
            }
        }

        let presenter = self.presentedViewController ?? self
        let alert = UIAlertController(title: "Error", message: "\(error)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        presenter.present(alert, animated: true, completion: nil)
    }
    
    @objc private func dismissLogViewer(sender: Any?)
    {
        self.dismiss(animated: true, completion: nil)
        _logViewer = nil
    }

    @objc private func cancelSync(sender: Any?)
    {
        self.cloudManager.cancel()
    }

}
