//
//  main.m
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-08-24.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
