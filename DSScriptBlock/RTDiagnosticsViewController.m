//
//  RTDiagnosticsViewController.m
//  Mileage
//
//  Created by Darren Stone on 2015-02-03.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

#import "RTDiagnosticsViewController.h"
#import "RTLogViewer.h"
#import "DSLog.h"
#import "AppDelegate.h"

@import MessageUI;

#pragma mark 1

typedef enum {
    eLogsSection,
    kNumSections
} Sections;

@interface RTDiagnosticsViewController()<MFMailComposeViewControllerDelegate>
@end

@implementation RTDiagnosticsViewController

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
        self.title = NSLocalizedString(@"DiagnosticsLabel", @"");
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(didPressMenuButton:)];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    _logFilePaths = DSLogPaths();
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case eLogsSection :
            return [_logFilePaths count];
    }
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case eLogsSection :
        {
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"eLogsSection"];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"eLogsSection"];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            cell.textLabel.text = [_logFilePaths[indexPath.row] lastPathComponent];
            return cell;
        }
    }
    NSAssert(NO, @"");
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case eLogsSection :
        {
            [self pushLogFile:_logFilePaths[indexPath.row]];
            break;
        }
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case eLogsSection :
            return NSLocalizedString(@"Log Files", @"");
    }
    return nil;
}

- (void)pushLogFile:(NSString*)logFilePath
{
    RTLogViewer* logViewer = [RTLogViewer new];
    logViewer.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(didPressMenuButton:)];
    logViewer.url = [NSURL fileURLWithPath:logFilePath];
    [self.navigationController pushViewController:logViewer animated:YES];
}

- (void)didPressMenuButton:(UIBarButtonItem*)sender
{
    NSAssert([sender isKindOfClass:[UIBarButtonItem class]], @"");
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Email", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self emailLogFiles];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

    }]];
    
    alert.modalPresentationStyle = UIModalPresentationPopover;
    alert.popoverPresentationController.barButtonItem = sender;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)emailLogFiles
{
    NSArray* logFiles = nil;
    if ([self.navigationController.topViewController isKindOfClass:[RTLogViewer class]])
    {
        RTLogViewer* logViewer = (id)self.navigationController.topViewController;
        if (logViewer.url)
        {
            logFiles = @[logViewer.url.path];
        }
    }
    if (!logFiles)
    {
        logFiles = _logFilePaths;
    }
    if ([logFiles count])
    {
        if (![g_appDelegate canSendMailFromPresentingViewController:self]) // Displays user message.
        {
            return;
        }

        MFMailComposeViewController* mailView = [[MFMailComposeViewController alloc] init];
        mailView.mailComposeDelegate = self;
        [mailView setSubject:[NSString stringWithFormat:@"DSScriptBlocker Log Files"]];
        [mailView setToRecipients:[NSArray arrayWithObject:@"support@darrensoft.ca"]];
        for (NSString* logFile in logFiles)
        {
            NSData* fileData = [[NSData alloc] initWithContentsOfFile:logFile];
            if (fileData)
            {
                [mailView addAttachmentData:fileData mimeType:@"text/plain" fileName:logFile.lastPathComponent];
            }
        }
        [self.navigationController presentViewController:mailView animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
