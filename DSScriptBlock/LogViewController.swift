//
//  LogViewController.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2016-10-14.
//  Copyright © 2016 Darren Stone. All rights reserved.
//

import UIKit

class LogViewController: UIViewController, DSLogWatcherDelegate
{
    private var _textView: UITextView?

    override func loadView()
    {
        _textView = UITextView()
        self.view = _textView
    }

    private var _logWatcher = DSLogWatcher()
    override func viewWillAppear(_ animated: Bool)
    {
        _logWatcher.delegate = self
        _logWatcher.enabled = true
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        _logWatcher.delegate = nil
        _logWatcher.enabled = false
    }

    func logMessage(_ text: String)
    {
        DispatchQueue.main.async {
            self._textView?.text = self._textView!.text + text + "\n"
        }
    }
}
