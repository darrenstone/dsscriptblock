//
//  RTLogViewer.m
//  Mileage
//
//  Created by Darren Stone on 2015-02-03.
//  Copyright (c) 2015 Darren Stone. All rights reserved.
//

#import "RTLogViewer.h"

@implementation RTLogViewer

- (void)loadView
{
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    _textView.editable = NO;
    _textView.font = [UIFont systemFontOfSize:12.0];
    if (_url)
    {
        [self loadUrl];
    }
    self.view = _textView;
}

- (void)setUrl:(NSURL *)url
{
    _url = url;
    if (_textView)
    {
        [self loadUrl];
    }
}
- (void)loadUrl
{
    if ([_url isFileURL])
    {
        self.title = [_url lastPathComponent];
        NSError* error = nil;
        NSString* contents = [[NSString alloc] initWithContentsOfURL:_url encoding:NSUTF8StringEncoding error:&error];
        if (!contents)
        {
            contents = [NSString stringWithFormat:@"Couldn't open log file: %@", error];
        }
        _textView.text = contents;
    }
    else
    {
        self.title = @"Untitled";
        _textView.text = @"";
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [_textView scrollRangeToVisible:NSMakeRange(_textView.text.length - 1, 1)];
    [super viewDidAppear:animated];
}

@end
