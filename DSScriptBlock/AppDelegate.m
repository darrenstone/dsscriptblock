//
//  AppDelegate.m
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-08-24.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

#import "AppDelegate.h"

#import "DSLog.h"
#import "DSScriptBlock-Swift.h"
#import "Reachability.h"

@import CloudKit;
@import UserNotifications;
@import MessageUI;

#pragma mark 1

AppDelegate* g_appDelegate = nil;

@interface AppDelegate ()

@end

@implementation AppDelegate

- (id)init
{
    if (self = [super init])
    {
        DSLogInit();
        g_appDelegate = self;
    }
    return self;
}

- (BlockerModel*)model
{
    if (!_model)
    {
        _model = [BlockerModel newSharedModel];
    }
    return _model;
}

- (CloudManager*)cloudManager
{
    if (!_cloudManager)
    {
        _cloudManager = [[CloudManager alloc] initWithModel:self.model];
    }
    return _cloudManager;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    DSLog(@"APP Launching...");

    _app = application;

    [application setMinimumBackgroundFetchInterval:60 * 60 * 24];
    [application registerForRemoteNotifications];

    UISplitViewController* splitViewController = (UISplitViewController*)self.window.rootViewController;
    splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    _listController = ((UINavigationController*)splitViewController.viewControllers[0]).viewControllers[0];
    NSAssert([_listController isKindOfClass:[UrlListController class]], @"");


    return YES;
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    completionHandler(UIBackgroundFetchResultNoData);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    DSLog(@"APP Did Enter Background");
    [BlockerModel saveSharedModel:_model];

    if (_cloudManager.needsSync)
    {
        DSLog(@"APP Starting background sync");
        UIBackgroundTaskIdentifier token = [application beginBackgroundTaskWithExpirationHandler:^{
            DSLog(@"APP Background task expired");
            [_cloudManager cancel];
        }];

        [_cloudManager beginBackgroundSyncWithCompletion:^{
            DSLog(@"APP Background Sync Complete");
            [application endBackgroundTask:token];
        }];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    DSLog(@"APP Will Enter Foreground");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler
{
    DSLogV(@"APP received remote notification: %@", userInfo);

    NSAssert(_listController != nil, @"");
    [_listController processRemoteNotification:userInfo completion:completionHandler];

//    CKNotification* note = [CKNotification notificationFromRemoteNotificationDictionary:userInfo];
//    if ([note.subscriptionID isEqual:@"UrlSubscription"])
//    {
//        
//    }
//    else
//    {
//        completionHandler(UIBackgroundFetchResultNoData);
//    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    DSLogV(@"APP Did register for notifications: %@", deviceToken);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    DSLogV(@"APP Failed to register for notifications: %@", error);
}


- (BOOL)canSendMailFromPresentingViewController:(UIViewController*)presentingController
{
    NSAssert(presentingController != nil, @"");

    if (![MFMailComposeViewController canSendMail])
    {
        NSString* deviceName = [UIDevice currentDevice].localizedModel;
        NSString* message = [NSString stringWithFormat:NSLocalizedString(@"This device is not configured to send email.", @""), deviceName, deviceName];

        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"No Email", @"") message:message preferredStyle:UIAlertControllerStyleAlert];

        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];

        [presentingController presentViewController:alert animated:YES completion:nil];

        return NO;
    }
    return YES;
}

@dynamic isInternetAvailable;
- (BOOL)isInternetAvailable
{
    if (!_reachability)
    {
        _reachability = [Reachability reachabilityForInternetConnection];
    }
    return _reachability.currentReachabilityStatus > 0;
}

@end
