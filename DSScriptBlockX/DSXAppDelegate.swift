//
//  AppDelegate.swift
//  DSScriptBlockX
//
//  Created by Darren Stone on 2018-06-20.
//  Copyright © 2018 Darren Stone. All rights reserved.
//

import Cocoa
import DSScriptBlockModelX
import CloudKit

// MARK: 1

@NSApplicationMain
class DSXAppDelegate: NSObject, NSApplicationDelegate, NSMenuItemValidation {

    @IBOutlet weak var window: NSWindow!
    @IBOutlet var windowController: DSXWindowController!
    @IBOutlet weak var _networkIndicator: NSProgressIndicator!

    lazy var model: BlockerModel = BlockerModel.newSharedModel()
    lazy var cloudManager: CloudManager = CloudManager(model: self.model)
    private var _logViewer: DSXLogViewer?
    

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        DSLogInit()
        DSLog("APP Launching...")
        NSApp.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        NotificationCenter.default.addObserver(self, selector: #selector(syncStatusDidChange(_:)), name: CloudManager.Notifications.syncStatusChanged, object: self.cloudManager)

        if self.cloudManager.enabled {
            self.cloudManager.scheduleSync(timeInterval: 1.5)
        }
    }

    private var _pendingQuit = false
    func applicationShouldTerminate(_ sender: NSApplication) -> NSApplication.TerminateReply {
        if !self.cloudManager.applicationShoudQuit() {
            _pendingQuit = true
            //sender.hide(self)
            return .terminateLater
        } else {
            return .terminateNow
        }
    }

    @objc private func syncStatusDidChange(_ sender: Any) {
        assert(Thread.current.isMainThread)
        if cloudManager.syncInProgress {
            _networkIndicator.startAnimation(self)
        } else {
            _networkIndicator.stopAnimation(self)
            self.saveModel()
            if _pendingQuit {
                NSApp.terminate(self)
            }
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        DSLog("APP Will Terminate")
        self.saveModel()
    }

    func saveModel() {
        BlockerModel.saveSharedModel(self.model)
    }

    func application(_ application: NSApplication, didReceiveRemoteNotification userInfo: [String : Any]) {
        DSLog("APP received remote notification: \(userInfo)")

        let note = CKNotification(fromRemoteNotificationDictionary: userInfo)!
        if note.subscriptionID == CloudUtil.subscriptionId {
            self.cloudManager.setNeedsFetch()
            DSLog("LST Background Sync beginning")
            self.cloudManager.scheduleSync(timeInterval: 0)
        }
    }

    func application(_ application: NSApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        DSLog("APP Did register for notifications: \(deviceToken)")
    }

    func application(_ application: NSApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        DSLog("APP Failed to register for notifications: \(error)")
    }

    @IBAction func didClickSyncNow(_ sender: Any) {
        self.beginSync(syncPolicy:.merge)
    }

    @IBAction func didClickSyncEnabled(_ sender: Any) {
        self.cloudManager.setEnabled(enabled: !self.cloudManager.enabled) { (error) in
            if let error = error {
                var message: String?
                var title: String?
                if error as? CloudManager.Error == CloudManager.Error.iCloudNotAvailable {
                    title = "Sign in to iCloud"
                    message = "You must first sign into iCloud"
                } else {
                    title = "Couldn't enable iCloud"
                    message = "Error: \(error)"
                }
                let alert = NSAlert()
                alert.messageText = title!
                alert.informativeText = message!
                alert.runModal()
                return
            }

            if self.cloudManager.enabled {
                let alert = NSAlert()
                alert.messageText = "Would you like to merge your local database with iCloud, or replace your database with iCloud?"
                alert.addButton(withTitle: "Merge")
                alert.addButton(withTitle: "Replace")
                alert.addButton(withTitle: "Cancel")
                let result = alert.runModal()
                switch result {
                case .alertFirstButtonReturn :
                    self.beginSync(syncPolicy:.merge)

                case .alertSecondButtonReturn :
                    self.beginSync(syncPolicy:.replace)

                default:
                    break
                }
            }
        }
    }

    @IBAction func showLogViewer(_ sender: Any) {
        if _logViewer == nil {
            let logViewer = DSXLogViewer(windowNibName: "DSXLogViewer")

            var token: AnyObject?
            token = NotificationCenter.default.addObserver(forName: NSWindow.willCloseNotification, object: logViewer.window, queue: nil) { [weak self] (note) in
                self?._logViewer = nil
                NotificationCenter.default.removeObserver(token!)
            }

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                DSLog("This is a log entry")
            }

            _logViewer = logViewer
        }
        _logViewer!.showWindow(self)
    }

    func validateMenuItem(_ menuItem: NSMenuItem) -> Bool {
        switch menuItem.action {
        case #selector(didClickSyncEnabled(_:)) :
            menuItem.state = self.cloudManager.enabled ? .on : .off
            return true;

        case #selector(didClickSyncNow(_:)) :
            return true;// self.cloudManager.enabled;

        default:
            return true
        }
    }

    private func beginSync(syncPolicy: CloudManager.SyncPolicy) {
        self.showLogViewer(self)
        if self.cloudManager.syncInProgress {
            self.cloudManager.cancel()
        }
        self.cloudManager.sync(syncPolicy: syncPolicy) { (error) in
            if let error = error {
                self.presentError(error)
            }
        }
    }

    private func presentError(_ error: Swift.Error) {
        if let ckError = error as? CKError {
            if ckError.code == .partialFailure {
                var skipAlert = true
                ckError.partialErrorsByItemID?.values.forEach({ (itemError) in
                    let itemError = itemError as! CKError
                    if itemError.code != .serverRecordChanged {
                        skipAlert = false
                    }
                })
                if skipAlert {
                    return
                }
            }
        }

        let alert = NSAlert(error: error)
        alert.runModal()
    }
}


