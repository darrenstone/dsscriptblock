//
//  DSXLogViewer.swift
//  DSScriptBlockX
//
//  Created by Darren Stone on 2018-06-21.
//  Copyright © 2018 Darren Stone. All rights reserved.
//

import Cocoa

class DSXLogViewer: NSWindowController, DSLogWatcherDelegate {

    override func windowDidLoad() {
        super.windowDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(stopWatching(_:)), name: NSWindow.willCloseNotification, object: self.window)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBOutlet var _textView: NSTextView!

    private var _logWatcher = DSLogWatcher()

    override func showWindow(_ sender: Any?) {
        super.showWindow(sender)
        startWatching()
    }

    private func startWatching() {
        _logWatcher.delegate = self
        _logWatcher.enabled = true
    }

    @objc private func stopWatching(_ sender: Any) {
        _logWatcher.delegate = nil
        _logWatcher.enabled = false
    }

    func logMessage(_ text: String) {
        DispatchQueue.main.async {
            self._textView.string += text + "\n"
        }
    }
}
