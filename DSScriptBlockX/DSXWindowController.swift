//
//  DSXWindowController.swift
//  DSScriptBlockX
//
//  Created by Darren Stone on 2018-06-21.
//  Copyright © 2018 Darren Stone. All rights reserved.
//

import Cocoa
import DSScriptBlockModelX
import SafariServices

@objc(DSXWindowController)
class DSXWindowController: NSWindowController, DSXUrlTableControllerDelegate {

    private var _whitelist: DSXUrlTableController!
    private var _blacklist: DSXUrlTableController!
    @IBOutlet private weak var _appDelegate: DSXAppDelegate!
    @IBOutlet private weak var _tabView: NSTabView!

    @objc dynamic private(set) var pluginStatus = ""
    @objc dynamic private(set) var debugStatus = ""
    @objc dynamic var enabled: Bool {
        get { return _appDelegate?.model.enabled ?? false }
        set(newVal) {
            if newVal != _appDelegate.model.enabled {
                _appDelegate.model.enabled = newVal
                _appDelegate.saveModel()
            }
        }
    }

    private var _hasSyncNotification = false

    override func awakeFromNib() {
        assert(_appDelegate != nil, "No app delegate")
        super.awakeFromNib()

        #if DEBUG
        debugStatus = "DEBUG BUILD"
        #endif

        _whitelist = DSXUrlTableController(nibName: "DSXUrlTable", bundle: nil)
        _tabView.tabViewItems[0].view = _whitelist.view
        _whitelist.urlType = .domainWhitelist
        _whitelist.delegate = self

        _blacklist = DSXUrlTableController(nibName: "DSXUrlTable", bundle: nil)
        _tabView.tabViewItems[1].view = _blacklist.view
        _blacklist.urlType = .domainBlacklist
        _blacklist.delegate = self

        if !_hasSyncNotification {
            _hasSyncNotification = true
            var notificationToken: NSObjectProtocol?
            notificationToken = NotificationCenter.default.addObserver(forName:CloudManager.Notifications.syncStatusChanged, object:_appDelegate.cloudManager, queue: OperationQueue.main, using: {
                [weak self] (note) in
                if let strongSelf = self {
                    let cloudManager = strongSelf._appDelegate.cloudManager
                    if !cloudManager.syncInProgress {
                        DSLog("LST Sync Complete")
                        if cloudManager.lastSyncHadChanges {
                            strongSelf.reload()
                        }
                    }
                } else {
                    NotificationCenter.default.removeObserver(notificationToken!)
                }
            })
        }

        self.updatePluginStatus()
        self.reload()
    }

    func reload() {
        assert(Thread.current.isMainThread)
        _whitelist?.blockerModel = _appDelegate.model
        _whitelist?.urls = _appDelegate.model.whitelistDomains
        _blacklist?.blockerModel = _appDelegate.model
        _blacklist?.urls = _appDelegate.model.blacklistDomains
    }

    func tableController(_ sender: DSXUrlTableController, didAddUrl url: UrlModel) {
        _appDelegate.model.addUrl(url)
        _appDelegate.saveModel()
        _appDelegate.cloudManager.scheduleSync(timeInterval: 1);
    }

    func tableController(_ sender: DSXUrlTableController, didChangeUrl url: UrlModel) {
        _appDelegate.model.isDirty = true
        _appDelegate.saveModel()
        _appDelegate.cloudManager.scheduleSync(timeInterval: 1);
    }

    func tableController(_ sender: DSXUrlTableController, didDeleteUrl url: UrlModel) {
        _appDelegate.model.removeUrl(url)
        _appDelegate.saveModel()
        _appDelegate.cloudManager.scheduleSync(timeInterval: 1);
    }

    private func updatePluginStatus() {
        SFContentBlockerManager.getStateOfContentBlocker(withIdentifier: BlockerModel.extensionIdentifier) { (state, error) in
            if let error = error {
                NSLog("Couldn't get plugin state: \(error)")
                self.pluginStatus = "Extension is not installed."
                return
            }

            if let state = state {
                if state.isEnabled {
                    self.pluginStatus = "Extension is installed and enabled."
                } else {
                    self.pluginStatus = "Extension is installed but not enabled."
                }
            }
        }
    }
}

