//
//  DSXAddUrlController.swift
//  DSScriptBlockX
//
//  Created by Darren Stone on 2020-04-04.
//  Copyright © 2020 Darren Stone. All rights reserved.
//

import Cocoa
import DSScriptBlockModelX

@objc(DSXAddUrlController)
@objcMembers
class DSXAddUrlController: NSWindowController {

    private var _blockerModel: BlockerModel!
    private var _urlModel: UrlModel! {
        didSet {
            self.domainKind = _urlModel.urlType
        }
    }
    dynamic var domain = ""
    dynamic var warning = ""
    dynamic var canSave = false
    dynamic var segmentedControlIndex = 0 {
        didSet {
            _urlModel.urlType = self.domainKind
            validateDomain()
        }
    }
    dynamic var urlText = "" {
        didSet {
            _urlModel.urlString = self.urlText
            validateDomain()
        }
    }

    private var domainKind: UrlModel.Kind {
        get {
            if self.segmentedControlIndex == 0 {
                return .domainWhitelist
            } else {
                return .domainBlacklist
            }
        }
        set(newVal) {
            switch newVal {
            case .domainBlacklist :
                self.segmentedControlIndex = 1
                break

            default :
                self.segmentedControlIndex = 0
                break
            }
        }
    }

    @IBOutlet weak var _textField: NSTextField!

    // --------------------------------------------------------------------
    //
    // MARK: - init
    //
    class func newController(blockerModel: BlockerModel, urlModel: UrlModel) -> DSXAddUrlController {
        let controller = DSXAddUrlController(windowNibName: "DSXAddUrlController")
        controller._blockerModel = blockerModel
        controller._urlModel = urlModel
        return controller
    }

    // --------------------------------------------------------------------
    //
    // MARK: - Public
    //

    public func generateUrlModel() -> UrlModel? {
        validateDomain()
        guard self.canSave else {
            return nil
        }

        _urlModel.urlString = self.domain
        return UrlModel(copy: _urlModel)
    }

    // --------------------------------------------------------------------
    //
    // MARK: - UI Actions
    //

    @IBAction func okAction(_ sender: Any) {
        NSApp.stopModal(withCode: .OK)
    }

    @IBAction func cancelAction(_ sender: Any) {
        NSApp.stopModal(withCode: .cancel)
    }

    // --------------------------------------------------------------------
    //
    // MARK: - Private
    //

    private func validateDomain() {

        let result: (String, BlockerModel.ValidationResult) = _blockerModel.validateUrl(model: _urlModel)
        self.domain = result.0
        switch result.1 {
        case .ok :
            self.canSave = true
            self.warning = ""
            break

        case .duplicateDomain :
            self.canSave = false
            self.warning = "Domain name already exists."
            break

        case .empty :
            self.canSave = false
            self.warning = "Please enter a domain name or URL"
            break

        case .invalidCharacters :
            self.canSave = false
            self.warning = "Domain contains invalid characters"
            break
        }
    }
}


