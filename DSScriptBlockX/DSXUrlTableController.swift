//
//  DSXUrlTableController.swift
//  DSScriptBlockX
//
//  Created by Darren Stone on 2018-06-24.
//  Copyright © 2018 Darren Stone. All rights reserved.
//

import Cocoa
import DSScriptBlockModelX

@objc(DSXUrlTableController)
class DSXUrlTableController : NSViewController, NSTableViewDelegate, NSTableViewDataSource {

    weak var delegate: DSXUrlTableControllerDelegate? = nil

    private var _urls: [UrlModel] = []

    var urls: [UrlModel] {
        set(newVal) {
            let selectedModel = _selectedModel
            _urls = newVal
            if let newEntry = _newEntry {
                _urls.insert(newEntry, at: 0)
            }
            resort();
            self.selectModel(selectedModel)
        }
        get { return _urls }
    }

    var blockerModel: BlockerModel?

    @IBAction func reload(_ sender: Any) {
        let selectedModel = _selectedModel
        resort();
        self.selectModel(selectedModel)
    }

    private func resort() {
        _urls.sort { (first: UrlModel, second: UrlModel) -> Bool in
            if _newEntry != nil {
                if first === _newEntry {
                    return true
                }
                if second === _newEntry {
                    return false
                }
            }
            return first.urlString < second.urlString
        }
        self.applyFilter()
        self.tableView?.reloadData()
    }

    private var _filteredUrls: [UrlModel]? = nil
    @IBOutlet var tableView: NSTableView!
    private var _newEntry: UrlModel?

    var urlType = UrlModel.Kind.domainWhitelist

    @objc dynamic var filter: String = "" {
        didSet {
            self.applyFilter()
            self.tableView.reloadData()
        }
    }

    private func applyFilter() {
        if filter.isEmpty {
            _filteredUrls = nil
        } else {
            let lowerFilter = filter.lowercased()
            _filteredUrls = _urls.filter { $0.urlString.lowercased().hasPrefix(lowerFilter) || $0 === _newEntry }
        }
    }

    private var _content: [UrlModel] {
        if let filtered = _filteredUrls {
            return filtered
        }
        return _urls
    }

    func numberOfRows(in tableView: NSTableView) -> Int {
        return _content.count
    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView
        let url = _content[row]
        cell?.objectValue = url
        cell?.textField?.stringValue = url.urlString
        return cell
    }


    private var _selectedModel: UrlModel? {
        let selectedRow = tableView.selectedRow
        guard selectedRow >= 0 else {
            return nil
        }
        return _content[selectedRow]
    }

    func selectModel(_ model: UrlModel?) {
        if let model = model {
            if let index = _content.firstIndex(of: model) {
                tableView.scrollRowToVisible(index)
                tableView.selectRowIndexes(IndexSet(integer:index), byExtendingSelection: false)
            }
        }
    }

    @IBAction func didEndEditing(_ sender: NSTextField) {
        let row = self.tableView.row(for: sender)
        guard row >= 0 && row < _content.count else {
            return
        }

        let url = _content[row]
        let isNewEntry = url === _newEntry
        _newEntry = nil

        let newText = sender.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        var didChange = false
        if !newText.isEmpty {
            if url.urlString != newText {
                url.urlString = newText
                didChange = true
            }
        }

        if newText.isEmpty && isNewEntry {
            self.deleteModel(url)
            return
        }

        self.resort()
        self.selectModel(url)

        if didChange {
            if isNewEntry {
                delegate?.tableController(self, didAddUrl: url)
            } else {
                assert(_newEntry == nil, "If newEntry exists, we shouldn't be editing another row")
                delegate?.tableController(self, didChangeUrl: url)
            }
        }
    }

    @IBAction func newEntry(_ sender: Any) {
        let urlModel = UrlModel()
        urlModel.urlType = self.urlType
        let controller = DSXAddUrlController.newController(blockerModel:self.blockerModel!, urlModel: urlModel)
//        controller.showWindow(self)
        if (NSApp.runModal(for: controller.window!) == .OK) {
            if let urlModel = controller.generateUrlModel() {
                if urlModel.urlType == self.urlType {
                    _urls.insert(urlModel, at: 0)
                    self.resort()
                    self.selectModel(urlModel)
                }
                delegate?.tableController(self, didAddUrl: urlModel)
            }
        }
        controller.close()
/*
        guard _newEntry == nil else {
            return
        }
        let newEntry = UrlModel()
        newEntry.urlType = self.urlType
        _newEntry = newEntry
        _urls.insert(newEntry, at: 0)
        _filteredUrls?.insert(newEntry, at: 0)
        self.tableView.reloadData()
        self.tableView.editColumn(0, row: 0, with: nil, select: true)
 */
    }

    private var _selectedTextField: NSTextField? {
        let index = self.tableView.selectedRow
        if index >= 0 {
            if let cell = self.tableView.view(atColumn: 0, row: index, makeIfNecessary: false) as? NSTableCellView {
                return cell.textField
            }
        }
        return nil
    }

    private func deleteModel(_ model: UrlModel) {
        if let index = _filteredUrls?.firstIndex(of: model) {
            _filteredUrls?.remove(at: index)
        }
        if let index = _urls.firstIndex(of: model) {
            _urls.remove(at: index)
        }
        self.tableView.deselectAll(self)
        self.tableView.reloadData()
    }

    @IBAction func deleteSelectedRow(_ sender: Any) {
        if let model = _selectedModel {
            _selectedTextField?.abortEditing()
            self.deleteModel(model)
            if model === _newEntry {
                _newEntry = nil
            } else {
                delegate?.tableController(self, didDeleteUrl: model)
            }
        }
    }
}

protocol DSXUrlTableControllerDelegate: class {
    func tableController(_ sender: DSXUrlTableController, didChangeUrl url: UrlModel);
    func tableController(_ sender: DSXUrlTableController, didAddUrl url: UrlModel);
    func tableController(_ sender: DSXUrlTableController, didDeleteUrl url: UrlModel);
}
