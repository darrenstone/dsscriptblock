//
//  DSScriptBlockModel.h
//  DSScriptBlockModel
//
//  Created by Darren Stone on 2015-09-05.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DSScriptBlockModel.
FOUNDATION_EXPORT double DSScriptBlockModelVersionNumber;

//! Project version string for DSScriptBlockModel.
FOUNDATION_EXPORT const unsigned char DSScriptBlockModelVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DSScriptBlockModel/PublicHeader.h>

