//
//  BlockerModelFactory.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import Foundation
import SafariServices

private let _v1ModelName = "ModelV1"
private let _modelName = "ModelV2"

extension BlockerModel
{
    public class func sharedUserDefaults() -> UserDefaults
    {
        return UserDefaults(suiteName: "group.ca.darrensoft.DSScriptBlock")!
    }

    public class func newSharedModel() -> BlockerModel
    {
        let defaults = self.sharedUserDefaults()
        if let v2model = defaults.dictionary(forKey: _modelName)
        {
            return BlockerModel(dictionary: v2model as [String : Any])
        }
        if let v1model = defaults.dictionary(forKey: _v1ModelName)
        {
            let model = BlockerModel(dictionary: v1model as [String : Any])
            model.upgradeV1Model()
            return model
        }
        return BlockerModel(dictionary: nil)
    }

    public class func saveSharedModel(_ model: BlockerModel)
    {
        let defaults = self.sharedUserDefaults()
        let dict = model.saveToDictionary()

        defaults.set(dict, forKey:_modelName)

        if model.isDirty
        {
            self.pingSafari()
            model.isDirty = false
        }

        do
        {
            let data = try model.jsonData()
            NSLog("%@", NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
        }
        catch let err as NSError
        {
            NSLog("Couldn't get JSON: %@", err)
        }
    }

    private class func pingSafari() {
        SFContentBlockerManager.reloadContentBlocker(withIdentifier: self.extensionIdentifier) { (error) in
            if let error = error
            {
                print("Error: \(error)")
            }
            else
            {
                print("Success")
            }
        }
    }

    public static var extensionIdentifier: String {
        #if os(iOS)
        return "ca.darrensoft.DSScriptBlock.DSScriptBlockExt"
        #else
        return "ca.darrensoft.DSScriptBlockX.DSScriptBlockExtX"
        #endif
    }
}
