//
//  DSUtil.h
//  DSScriptBlock
//
//  Created by Darren Stone on 2016-10-30.
//  Copyright © 2016 Darren Stone. All rights reserved.
//

#ifndef DSUtil_h
#define DSUtil_h

#define RT_STRINGIFY(s) #s
#define RT_PRAGMA_UNUSED_VAR(varname) _Pragma(RT_STRINGIFY(unused(varname)))

#define RT_WARNINGS_BEGIN_IGNORE _Pragma("clang diagnostic push")
#define RT_WARNINGS_IGNORE_USUSED_VARS _Pragma("clang diagnostic ignored \"-Wunused-variable\"")
#define RT_WARNINGS_IGNORE_UNREACHABLE_CODE _Pragma("clang diagnostic ignored \"-Wunreachable-code\"")
#define RT_WARNINGS_IGNORE_PERFORM_SELECTOR_LEAKS _Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"")
#define RT_WARNINGS_IGNORE_DEPRECATED_IMPLEMENTATIONS _Pragma("clang diagnostic ignored \"-Wdeprecated-implementations\"")
#define RT_WARNINGS_IGNORE_DEPRECATED_DECLARATIONS _Pragma("clang diagnostic ignored \"-Wdeprecated-declarations\"")
#define RT_WARNINGS_IGNORE_STRICT_SELECTOR_MATCH _Pragma("clang diagnostic ignored \"-Wstrict-selector-match\"")
#define RT_WARNINGS_IGNORE_DESIGNATED_INIT _Pragma("clang diagnostic ignored \"-Wobjc-designated-initializers\"")
#define RT_WARNINGS_END_IGNORE _Pragma("clang diagnostic pop")


#endif /* DSUtil_h */
