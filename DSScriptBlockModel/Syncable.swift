//
//  Syncable.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import Foundation

public protocol Syncable : Hashable, CustomStringConvertible
{
    var uuid: String { get }
    var lastMod: TimeInterval { get }
    var deleted: Bool { get }

    func updateLastMod()
}

extension Syncable
{
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.uuid)
    }
}

public func ==<T: Syncable>(x: T, y: T) -> Bool
{
    return x.uuid == y.uuid
}
