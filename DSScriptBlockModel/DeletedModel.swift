//
//  DeletedModel.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import Foundation
import CloudKit

open class DeletedModel : SyncableModel
{
    open override var deleted: Bool { return true }
    open var ckRecordId: CKRecord.ID

    public init(ckRecordId: CKRecord.ID)
    {
        self.ckRecordId = ckRecordId
        super.init(uuid: ckRecordId.recordName)
        self.updateLastMod()
    }

    struct Keys {
        static let ckRecordId = "CKRecordId"
    }

    public override init?(dictionary dict: [String : Any])
    {
        var ckRecordIdP: CKRecord.ID?
        if let ckRecordIdData = dict[Keys.ckRecordId] as? Data
        {
            let coder = NSKeyedUnarchiver(forReadingWith: ckRecordIdData)
            if let ckRecordIdPlist = CKRecord.ID(coder: coder)
            {
                ckRecordIdP = ckRecordIdPlist
            }
        }

        guard let ckRecordId = ckRecordIdP else
        {
            return nil
        }

        self.ckRecordId = ckRecordId
        super.init(dictionary: dict)
    }
}
