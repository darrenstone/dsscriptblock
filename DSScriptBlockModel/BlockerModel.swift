//
//  BlockerModel.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import Foundation
import CloudKit
import SafariServices

@objcMembers
open class BlockerModel : NSObject
{
    private var _urls = [String:UrlModel]()
    private var _deletedUrls = [String:DeletedModel]()

    open var enabled = true {
        didSet { isDirty = true }
    }
    open var isDirty = false

    private struct Keys
    {
        static let urls = "Urls"
        static let deletedUrls = "DeletedUrls"
        static let version = "Version"
        static let disabled = "Disabled"
    }

    open var needsCloudSync: Bool
    {
        return self.changelist().needsCloudSync
    }

    public init(dictionary dictionaryP: [String: Any]?)
    {
        super.init()

        if let dictionary = dictionaryP
        {
            if let urls = dictionary[Keys.urls] as? [ [String: Any] ]
            {
                for urlDict in urls
                {
                    if let urlModel = UrlModel(dictionary: urlDict)
                    {
                        _urls[urlModel.uuid] = urlModel
                    }
                }
            }

            if let urls = dictionary[Keys.deletedUrls] as? [ [String: Any] ]
            {
                for urlDict in urls
                {
                    if let deletedModel = DeletedModel(dictionary:urlDict)
                    {
                        _deletedUrls[deletedModel.uuid] = deletedModel
                    }
                }
            }

            if let disabled = dictionary[Keys.disabled] as? Bool
            {
                self.enabled = !disabled
            }
        }

        if _urls.count == 0
        {
            self.loadDefaults()
        }
    }

    open func saveToDictionary() -> [String: Any]
    {
        let urls = _urls.values.map { (url) -> [String: Any] in
            return url.saveToDictionary()
        }

        let deletedUrls = _deletedUrls.values.map { (url) -> [String:Any] in
            return url.saveToDictionary()
        }

        return [
            Keys.version : 100,
            Keys.urls : Array(urls) ,
            Keys.deletedUrls : Array(deletedUrls),
            Keys.disabled : !self.enabled
        ]
    }

    open func purge()
    {
        _urls.removeAll()
        _deletedUrls.removeAll()
    }

    open var blacklistUrls : [UrlModel] { return self.urlsOfType(.urlBlacklist) }

    open var blacklistDomains : [UrlModel] { return self.urlsOfType(.domainBlacklist) }

    open var whitelistDomains : [UrlModel] { return self.urlsOfType(.domainWhitelist) }

    open var deletedEntries : [DeletedModel] { return Array(_deletedUrls.values) }

    open func urlWithId(_ uid: String) -> UrlModel? {
        return _urls[uid]
    }

    open func addUrl(_ url: UrlModel)
    {
        _urls[url.uuid] = UrlModel(copy: url)
        self.isDirty = true
    }

    open func removeUrl(_ url: UrlModel)
    {
        _urls.removeValue(forKey: url.uuid)
        let deleted = DeletedModel(ckRecordId: url.ckRecord.recordID)
        _deletedUrls[deleted.uuid] = deleted
        self.isDirty = true
    }

    public enum ValidationResult: Error {
        case ok
        case empty
        case duplicateDomain
        case invalidCharacters
    }

    /// Returns normalized domain name from url string
    open func validateUrl(model: UrlModel) -> (String, ValidationResult)  {
        let text = model.urlString.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
        if text.isEmpty {
            return ("", ValidationResult.empty)
        }

        var domain = ""
        if let urlHost = URL(string:text)?.host {
            domain = urlHost
        }
        else {
            domain = text.strippingPathComponents()
        }

        if let range = domain.range(of: "www."), range.lowerBound == domain.startIndex {
            domain.removeSubrange(range)
        }

        if domain.isEmpty {
            return ("", ValidationResult.empty)
        }

        if domain.hasInvalidUrlHostChars {
            return (domain, ValidationResult.invalidCharacters)
        }

        let existingModels = model.urlType == .domainWhitelist ? self.whitelistDomains : self.blacklistDomains
        for existingModel in existingModels {
            if existingModel.uuid == model.uuid {
                continue
            }
            if existingModel.urlString == domain {
                return (domain, ValidationResult.duplicateDomain)
            }
        }

        return (domain, ValidationResult.ok)
    }

    open func jsonData() throws -> Data
    {
        let rules = [ self.domainRulesWithUrls(self.blacklistDomains, whitelisted:false),
            self.domainRulesWithUrls(self.whitelistDomains, whitelisted:true) ]

        let data = try JSONSerialization.data(withJSONObject: rules, options: JSONSerialization.WritingOptions())
        return data
    }

    public struct Changelist
    {
        public var modifiedRecords: [UrlModel]
        public var deletedRecords: [DeletedModel]

        public var needsCloudSync: Bool {
            for url in modifiedRecords {
                if url.needsCloudSync {
                    return true
                }
            }
            for url in deletedRecords
            {
                if url.needsCloudSync {
                    return true
                }
            }
            return false
        }
    }

    public func changelist() -> Changelist
    {
        let urls = Array(_urls.values.filter { $0.needsCloudSync })
        let deletedUrls = Array(_deletedUrls.values.filter { $0.needsCloudSync })

        return Changelist(modifiedRecords: urls, deletedRecords:deletedUrls)
    }

    public func resolveConflict(ancestor: CKRecord?, server: CKRecord?, client: CKRecord?) -> CKRecord?
    {
        guard let server = server, let record = _urls[server.recordID.recordName] else
        {
            return nil
        }

        record.merge(ckRecord: server, mergePolicy: .newerWins)
        self.isDirty = true

        if record.needsCloudSync
        {
            return record.ckRecord
        }
        else
        {
            return nil
        }
    }

    public func deletedEntry(forId id:CKRecord.ID) -> DeletedModel?
    {
        return _deletedUrls[id.recordName]
    }

    public func process(changedCkRecord ckRecord:CKRecord)
    {
        if let record = self.record(forId: ckRecord.recordID)
        {
            record.merge(ckRecord: ckRecord, mergePolicy: .newerWins)
        }
        else
        {
            let newRecord = UrlModel(ckRecord: ckRecord)
            self.addUrl(newRecord)
        }
        self.isDirty = true
    }

    public func process(deletedRecordId id: CKRecord.ID)
    {
        if let record = self.record(forId: id)
        {
            self.removeUrl(record)
        }

        if _deletedUrls[id.recordName] != nil
        {
            _deletedUrls.removeValue(forKey: id.recordName)
            self.isDirty = true
        }
    }

    public func initialMerge(changedRecords: [CKRecord], deletedRecordIds: [CKRecord.ID]) -> Bool
    {
        // Step 1: Reset the sync state of all records
        _deletedUrls.removeAll()
        _urls.forEach { $0.1.needsCloudSync = true }
        self.isDirty = true

        // Step 2: Remove any matching deleted records
        for deletedRecordId in deletedRecordIds
        {
            _urls.removeValue(forKey: deletedRecordId.recordName)
        }
        var unprocessedLocalRecordMap = _urls

        // Step 3: Merge records matching UUID
        var changedRecordMap = [String: CKRecord]()
        changedRecords.forEach { changedRecordMap[$0.recordID.recordName] = $0 }

        for changedRecord in changedRecords
        {
            if let matchingRecord = _urls[changedRecord.recordID.recordName]
            {
                matchingRecord.merge(ckRecord: changedRecord, mergePolicy: .newerWins)
                changedRecordMap.removeValue(forKey: changedRecord.recordID.recordName)
                unprocessedLocalRecordMap.removeValue(forKey: matchingRecord.uuid)
            }
        }

        // Step 4: Try matching by URL (Remove any matching records.  They will be re-added in Step 5)
        var urlNameMap = [String: UrlModel]()
        _urls.values
            .filter { return $0.needsCloudSync }
            .forEach { urlNameMap[$0.urlString.lowercased()] = $0 }

        if urlNameMap.count > 0
        {
            for changedRecord in changedRecordMap.values
            {
                if let remoteName = changedRecord[UrlModel.Keys.url] as? String
                {
                    if let matchingUrl = urlNameMap[remoteName.lowercased()]
                    {
                        _urls.removeValue(forKey: matchingUrl.uuid)
                        unprocessedLocalRecordMap.removeValue(forKey: matchingUrl.uuid)
                    }
                }
            }
        }

        // Step 5: Add all unmatched records
        for newRecord in changedRecordMap.values
        {
            let newUrl = UrlModel(ckRecord: newRecord)
            newUrl.needsCloudSync = false
            _urls[newUrl.uuid] = newUrl
        }

        // At this point, records with needsCloudSync == false are in sync with the server.  
        // Records with needsCloudSync == true will be sent during the next normal sync.
        for url in unprocessedLocalRecordMap.values
        {
            assert(url.needsCloudSync)
            url.resetCkRecord()
            url.needsCloudSync = true
        }

        for url in _urls.values
        {
            if url.needsCloudSync
            {
                return true
            }
        }

        return false
    }

    private func record(forId id: CKRecord.ID) -> UrlModel?
    {
        return _urls[id.recordName]
    }
}

extension CKRecord
{
    var lastMod: TimeInterval {
        return self.modificationDate?.timeIntervalSinceReferenceDate ?? 0
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// MARK: - Private
//

extension BlockerModel
{
    private func domainRulesWithUrls(_ urls: [UrlModel], whitelisted: Bool) -> [String:Any]
    {
        let domainKey = whitelisted ? "unless-domain" : "if-domain"
        let urlStrings = self.urlStringsFromModelArray(urls, strict:!whitelisted)

        return [
            "trigger" : [ "resource-type" : ["script"], "url-filter" : ".*" , domainKey : urlStrings ],
            "action" : [ "type" : "block" ] ]
    }

    private func urlStringsFromModelArray(_ modelArray: [UrlModel], strict: Bool) -> [String]
    {
        var strings = [String]()
        for model in modelArray
        {
            let urlString = model.urlString
            strings.append(urlString)
            if !strict && urlString.range(of: "*") == nil
            {
                strings.append("*\(urlString)")
            }
        }
        return strings
    }

    private func defaultWhitelist() -> [(String, String)]
    {
        return [("b3","reddit.com"), ("b4","stackoverflow.com"), ("b5","google.com"), ("b6","google.ca"), ("b7","apple.com"), ("b8","firesmoke.ca"), ("b9", "royalbank.com"), ("b10", "americanexpress.com"), ("b11", "netflix.com"), ("b12", "darrensoft.ca"), ("b13", "audry.local"), ("b14", "m.mlb.com"), ("b15", "canadapost.ca"), ("b16", "bchydro.com"), ("b17", "youtube.com"), ("b18", "vimeo.com"), ("b19", "imgur.com"), ("b20", "ebay.com"), ("b21", "ebay.ca"), ("b22", "amazon.com"), ("b23", "amazon.ca"), ("b24", "fastmail.com"), ("b25", "pair.com"), ("b26", "metafilter.com"), ("b27", "tumblr.com"), ("b28", "wunderground.com"), ("b29", "feedly.com"), ("b30", "ycombinator.com"), ("b31", "fortisbc.com")]
    }

    private func defaultBlacklist() -> [(String, String)]
    {
        return [("b1","news.google.com"), ("b2","news.google.ca")]
    }

    private func loadDefaults()
    {
        _urls.removeAll()
        self.addUrls(self.defaultBlacklist(), type: .domainBlacklist)
        self.addUrls(self.defaultWhitelist(), type: .domainWhitelist)
    }

    private func addUrls(_ urls: [(String, String)], type: UrlModel.Kind)
    {
        for url in urls
        {
            let urlModel = UrlModel(uuid: url.0)
            urlModel.urlString = url.1
            urlModel.urlType = type
            _urls[urlModel.uuid] = urlModel
            self.isDirty = true
        }
    }

    private func urlsOfType(_ urlType: UrlModel.Kind) -> [UrlModel]
    {
        let urls = _urls.values.filter { $0.urlType == urlType }
        return urls.sorted { $0 < $1 }
    }

    public func upgradeV1Model()
    {
        var defaultUrlMap = [String: String]()

        for urlPair in self.defaultWhitelist()
        {
            defaultUrlMap[urlPair.1] = urlPair.0
        }

        for urlPair in self.defaultBlacklist()
        {
            defaultUrlMap[urlPair.1] = urlPair.0
        }

        let existingUrls = Array(_urls.values)
        for url in existingUrls
        {
            if let newUuid = defaultUrlMap[url.urlString]
            {
                let newUrl = UrlModel(uuid: newUuid)
                newUrl.urlString = url.urlString
                newUrl.urlType = url.urlType
                newUrl.notes = url.notes
                _urls.removeValue(forKey: url.uuid)
                _urls[newUrl.uuid] = newUrl
                self.isDirty = true
            }
        }
    }

}

private extension String {
    func strippingPathComponents() -> String {
        if let index = self.firstIndex(of: "/") {
            return String(self[..<index])
        }
        return self
    }

    var hasInvalidUrlHostChars: Bool {
        let invalidChars = CharacterSet.urlHostAllowed.inverted
        return self.contains(where: { (char: Character) -> Bool in
            for scalar in char.unicodeScalars {
                if invalidChars.contains(scalar) {
                    return true
                }
            }
            return false
        })
    }
}
