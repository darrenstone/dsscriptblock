//
//  CloudUtil.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2016-10-13.
//  Copyright © 2016 Darren Stone. All rights reserved.
//

import Foundation
import CloudKit

public class CloudUtil
{
    public struct RecordType
    {
        public static let urlModel = "UrlRecord"
        public static let defaultCollection = "DefaultCollection"
    }

    public static let urlZoneId = CKRecordZone.ID(zoneName: "UrlZone", ownerName: CKCurrentUserDefaultName)
    public static let defaultCollectionId = CKRecord.ID(recordName: "DefaultCollection", zoneID: urlZoneId)
    public static let subscriptionId = "UrlSubscription"

    public static func newUrlRecord(uuid: String) -> CKRecord
    {
        let record = CKRecord(recordType: RecordType.urlModel, recordID: CKRecord.ID(recordName: uuid, zoneID: urlZoneId))
        record["ParentCollection"] = CKRecord.Reference(recordID: defaultCollectionId, action: .deleteSelf)
        return record
    }
}
