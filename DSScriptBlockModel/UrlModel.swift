//
//  UrlModel.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import Foundation
import CloudKit

open class UrlModel : SyncableModel, Comparable
{
    public struct Keys
    {
        public static let url = "URL"
        public static let urlType = "UrlType"
        fileprivate static let ckRecord = "CKRecord"
        public static let notes = "Notes"
    }

    public enum Kind : Int
    {
        case domainWhitelist = 0, domainBlacklist, urlBlacklist
    }

    public static let ckRecordType = "UrlRecord"

    private var _urlString = ""
    private var _urlType = Kind.domainWhitelist
    private var _notes = ""

    open var urlString: String {
        get { return _urlString }
        set(newVal) {
            _urlString = newVal
            self.ckRecord[Keys.url] = _urlString as NSString
            self.needsCloudSync = true
        }
    }

    open var urlType: Kind {
        get { return _urlType }
        set(newVal) {
            _urlType = newVal
            self.ckRecord[Keys.urlType] = _urlType.rawValue as NSNumber
            self.needsCloudSync = true
        }
    }

    open var notes: String {
        get { return _notes }
        set(newVal) {
            _notes = newVal
            self.ckRecord[Keys.notes] = _notes as NSString
            self.needsCloudSync = true
        }
    }

    public var ckRecord: CKRecord

    public convenience init()
    {
        let uuid = UUID().uuidString
        self.init(uuid: uuid)
    }

    public override init(uuid: String)
    {
        self.ckRecord = CloudUtil.newUrlRecord(uuid: uuid)
        super.init(uuid: uuid)

        self.needsCloudSync = true
    }

    public init(ckRecordId id: CKRecord.ID)
    {
        self.ckRecord = CKRecord(recordType: UrlModel.ckRecordType, recordID: id)
        super.init(uuid: id.recordName)
    }


    public init(ckRecord: CKRecord)
    {
        assert(ckRecord.recordType == UrlModel.ckRecordType)

        self.ckRecord = ckRecord
        super.init(uuid: ckRecord.recordID.recordName)
        self.merge(ckRecord: ckRecord, mergePolicy: .cloudWins)
    }

    public convenience init(url: String, type: Kind)
    {
        self.init()
        self.urlString = url
        self.urlType = type
    }

    private static let _placeholderRecord = CKRecord(recordType: "Placeholder")

    public override init?(dictionary dict: [String : Any])
    {
        var plistCkRecord: CKRecord?
        if let ckRecordData = dict[Keys.ckRecord] as? Data
        {
            let archiver = NSKeyedUnarchiver(forReadingWith: ckRecordData)
            plistCkRecord = CKRecord(coder: archiver)
        }

        if let plistCkRecord = plistCkRecord
        {
            self.ckRecord = plistCkRecord
        }
        else
        {
            self.ckRecord = UrlModel._placeholderRecord
        }

        super.init(dictionary: dict)

        let urlStringP = dict[Keys.url] as? String
        let urlTypeValP = dict[Keys.urlType] as? Int
        let urlTypeP = Kind(rawValue:urlTypeValP ?? -1)

        guard let urlString = urlStringP, let urlType = urlTypeP else
        {
            return nil
        }

        _urlString = urlString
        _urlType = urlType
        _notes = dict[Keys.notes] as? String ?? ""

        if plistCkRecord == nil
        {
            self.ckRecord = CloudUtil.newUrlRecord(uuid: self.uuid)
            self.applyTo(ckRecord:self.ckRecord)
            self.needsCloudSync = true
        }
    }

    public init(copy: UrlModel)
    {
        _urlString = copy.urlString
        _urlType = copy.urlType
        _notes = copy.notes
        self.ckRecord = copy.ckRecord.copy() as! CKRecord
        super.init(copy:copy)
    }

    open override func saveToDictionary() -> [String : Any]
    {
        var dict = super.saveToDictionary()
        dict[Keys.url] = _urlString
        dict[Keys.urlType] = _urlType.rawValue
        dict[Keys.notes] = _notes
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        if self.needsCloudSync || self.ckRecord.changedKeys().count > 0
        {
            self.ckRecord.encode(with: archiver)
        }
        else
        {
            self.ckRecord.encodeSystemFields(with: archiver)
        }
        archiver.finishEncoding()
        dict[Keys.ckRecord] = data
        return dict
    }

    open func compare(_ other: UrlModel) -> ComparisonResult
    {
        return self.urlString.compare(other.urlString)
    }

    open func matches(model: UrlModel) -> Bool {
        return model.urlString == self.urlString &&
            model.urlType == self.urlType &&
            model.notes == self.notes
    }

    open func matches(ckRecord: CKRecord) -> Bool
    {
        let recordUrl = ckRecord[Keys.url] as? String ?? ""
        let recordType = ckRecord[Keys.urlType] as? Int ?? 0
        let recordNotes = ckRecord[Keys.notes] as? String ?? ""

        return recordUrl == _urlString &&
            recordType == _urlType.rawValue &&
            recordNotes == _notes
    }

    open func applyTo(ckRecord: CKRecord)
    {
        ckRecord[Keys.url] = _urlString as NSString
        ckRecord[Keys.urlType] = _urlType.rawValue as NSNumber
        ckRecord[Keys.notes] = _notes as NSString
    }

    public enum MergePolicy {
        case cloudWins
        case newerWins
    }

    open func merge(ckRecord: CKRecord, mergePolicy: MergePolicy = .newerWins)
    {
        assert(ckRecord.recordType == "UrlRecord")
        assert(ckRecord.recordID.recordName == self.uuid)

        guard ckRecord.recordID.recordName == self.uuid && ckRecord.recordType == UrlModel.ckRecordType else
        {
            return
        }

        if mergePolicy == .newerWins && self.lastMod > ckRecord.lastMod
        {
            self.applyTo(ckRecord: ckRecord)
            self.ckRecord = ckRecord
            self.needsCloudSync = true
        }
        else
        {
            if let url = ckRecord[Keys.url] as? String
            {
                _urlString = url
            }
            if let urlTypeValue = ckRecord[Keys.urlType] as? Int
            {
                if let urlType = Kind(rawValue: urlTypeValue)
                {
                    _urlType = urlType
                }
            }
            if let notes = ckRecord[Keys.notes] as? String
            {
                _notes = notes
            }
            self.ckRecord = ckRecord
            self.lastMod = ckRecord.lastMod
            self.needsCloudSync = false
        }
    }

    open func resetCkRecord()
    {
        self.ckRecord = CloudUtil.newUrlRecord(uuid: self.uuid)
        self.applyTo(ckRecord: self.ckRecord)
    }

    open override var description: String
    {
        return "\(super.description) url:\(self.urlString) type:\(self.urlType)"
    }    

}

public func <(x: UrlModel, y: UrlModel) -> Bool
{
    return x.urlString.compare(y.urlString, options: NSString.CompareOptions.caseInsensitive) == .orderedAscending
    //return x.urlString < y.urlString
}

