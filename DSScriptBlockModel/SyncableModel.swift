//
//  SyncableModel.swift
//  DSScriptBlock
//
//  Created by Darren Stone on 2015-09-06.
//  Copyright © 2015 Darren Stone. All rights reserved.
//

import Foundation
import CloudKit

open class SyncableModel : Syncable
{
    public let uuid: String
    public var lastMod: TimeInterval = 0
    public var deleted: Bool { return false }
    public var needsCloudSync = false

    private struct Keys
    {
        static let uuid = "UUID"
        static let lastMod = "LastMod"
        static let needsCloudSync = "NeedsCloudSync"
    }

    public init(uuid: String = UUID().uuidString)
    {
        self.uuid = uuid
        self.needsCloudSync = true
    }

    public init?(dictionary dict: [String : Any])
    {
        let uuidP = dict[Keys.uuid] as? String
        let lastModP = dict[Keys.lastMod] as? TimeInterval
        let needsCloudSyncP = dict[Keys.needsCloudSync] as? Bool

        guard let uuid = uuidP else
        {
            self.uuid = ""
            return nil
        }

        self.uuid = uuid
        if let lastMod = lastModP
        {
            self.lastMod = lastMod
        }
        self.needsCloudSync = needsCloudSyncP ?? true
    }

    public init(copy: SyncableModel)
    {
        self.uuid = copy.uuid
        self.lastMod = copy.lastMod
        self.needsCloudSync = copy.needsCloudSync
    }

    open func saveToDictionary() -> [String : Any]
    {
        let dict: [String:Any] = [Keys.uuid : self.uuid, Keys.lastMod: self.lastMod, Keys.needsCloudSync: self.needsCloudSync]
        return dict
    }

    open func updateLastMod()
    {
        self.lastMod = Date().timeIntervalSinceReferenceDate
    }

    open var description: String {
        return "\(type(of: self)): \(Unmanaged.passUnretained(self).toOpaque()) \(self.uuid) \(self.lastMod) \(self.needsCloudSync)"
    }
}
