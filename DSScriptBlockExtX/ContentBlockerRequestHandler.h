//
//  ContentBlockerRequestHandler.h
//  DSScriptBlockExtX
//
//  Created by Darren Stone on 2018-06-22.
//  Copyright © 2018 Darren Stone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentBlockerRequestHandler : NSObject <NSExtensionRequestHandling>

@end
