//
//  ContentBlockerRequestHandler.m
//  DSScriptBlockExtX
//
//  Created by Darren Stone on 2018-06-22.
//  Copyright © 2018 Darren Stone. All rights reserved.
//

#import "ContentBlockerRequestHandler.h"

@import DSScriptBlockModelX;

@interface ContentBlockerRequestHandler ()

@end

@implementation ContentBlockerRequestHandler

- (void)beginRequestWithExtensionContext:(NSExtensionContext *)context
{
    NSItemProvider* attachment = nil;

    BlockerModel* model = [BlockerModel newSharedModel];
    if (model.enabled)
    {
        NSData* jsonData = [model jsonDataAndReturnError:nil];

        if (jsonData)
        {
            attachment = [[NSItemProvider alloc] initWithItem:jsonData typeIdentifier:(id)kUTTypeJSON];
        }
        else
        {
            attachment = [[NSItemProvider alloc] initWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"blockerList" withExtension:@"json"]];

        }

    }
    else
    {
        attachment = [[NSItemProvider alloc] initWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"disabled" withExtension:@"json"]];
    }

    NSExtensionItem *item = [[NSExtensionItem alloc] init];
    item.attachments = @[attachment];
    [context completeRequestReturningItems:@[item] completionHandler:nil];
}

@end
